app.controller('profileController', function($timeout, $http, $rootScope, $window, $scope, $sce) {
    var getMfiLoginData = JSON.parse(localStorage.getItem("mfiInfo"));
    $scope.Postformpage = $sce.trustAsResourceUrl(domainAddressImage);
    $scope.domainurl = $sce.trustAsResourceUrl(domainAddress);
    var userID = 0;
    var companyID = 0;

    var userType = '';
    var imageUrl1 = "";
    
    if (getMfiLoginData == null) {
        window.location.href = "#!/Login";
    } else {
        $scope.userID = getMfiLoginData[0].UserID;
        $scope.UserName = getMfiLoginData[0].UserName;
        $scope.UserPhoneNumber = getMfiLoginData[0].UserPhoneNumber;
        $scope.UserEmailID = getMfiLoginData[0].UserEmailID;
        $scope.UserAddress = getMfiLoginData[0].UserAddress;
        $scope.UserPhoto = getMfiLoginData[0].UserPhoto;
        $scope.UserType = getMfiLoginData[0].UserType;
        $scope.UserApplicationAmount = getMfiLoginData[0].UserApplicationAmount;
        userType = getMfiLoginData[0].UserType;
        if (userType == 'SuperAdmin') {
            UserCompanyID = getMfiLoginData[0].UserCompanyID;
            UserIsActive = getMfiLoginData[0].UserIsActive;
            $rootScope.isForSuperAdmin = true;
            $rootScope.isAdmin = false;
        } else if (userType == 'Admin') {
            UserCompanyID = getMfiLoginData[0].UserCompanyID;
            UserIsActive = getMfiLoginData[0].UserIsActive;
            companyID = getMfiLoginData[0].Company[0].CompanyID;
            CompanyName = getMfiLoginData[0].Company[0].CompanyName;
            CompanyIsActive = getMfiLoginData[0].Company[0].CompanyIsActive;
            CompanyCreatedON = getMfiLoginData[0].Company[0].CompanyCreatedON;
            $rootScope.isForSuperAdmin = false;
            $rootScope.isAdmin = true;
        }
 
        if ($scope.UserPhoto == "" || $scope.UserPhoto == null) {
            $("#imgAdminImage").attr("src", "../assets/common/img/noImage.gif");
            imageUrl1 = "";
        } else {
            imageUrl1 = $scope.UserPhoto;
            $("#imgAdminImage").attr("src", domainAddress + imageUrl1);
        } 
        window.location.href = "#!/admin/profile";
    }


    /// image upload

    $("#memberImageUrl1").off('click').on('change', function() {
        var progressbox = $('#progressbox1');
        var progressbar = $('#progressbar1');
        var statustxt = $('#statustxt1');

        $("#preview1").html('');

        $("#FileURLUploadImage1").ajaxForm({
            target: '#preview1',
            beforeSubmit: function() {

            },
            uploadProgress: function(event, position, total, percentComplete) {
                progressbar.width(percentComplete + '%') //update progressbar percent complete
                statustxt.html(percentComplete + '%'); //update status text
                $('#progressbar1').css("width", percentComplete + "%");
                $('#progressbox1').css("margin", "10px 10px 10px 20px");
                $('#progressbox1').show();
                $('#progressbar1').show();

                if (percentComplete > 50) {
                    statustxt.css('color', '#fff');
                    statustxt.html(percentComplete + '%'); //change status text to white after 50%
                    $('#progressbar1').css("width", percentComplete + "%");
                    $('#progressbox1').css("margin", "10px 10px 10px 20px");
                    $('#progressbox1').show();
                    $('#progressbar1').show();
                }
                $("#tableButtonType").attr("disabled", true);
            },
            success: function(result, percentComplete) {
                if (result == "Please select image..!") {
                    $("#progressbox1").hide();
                    alert("Please select image..!");
                    return false;
                } else if (result == "Invalid file format") {
                    $("#Imagealertdiv").modal('show');
                    $(".memberText").text("Upload only JPG or PNG file format");
                    return false;
                } else if (result == "Image file size max 1 MB") {
                    $("#Imagealertdiv").modal('show');
                    $(".memberText").text("Upload Image file sixe less then 1 MB");
                    return false;
                } else {
                    if (percentComplete == "success") {
                        percentComplete = 100;
                        statustxt.html(percentComplete + ' %');
                    }

                    $('#progressbar1').css("width", percentComplete + "%");
                    $('#progressbox1').css("margin", "10px 10px 10px 20px");
                    $('#progressbox1').show();
                    $('#progressbar1').show();
                    var getUrl = "url(" + result + ")";
                    imageUrl1 = result;
                    $("#tableButtonType").attr("disabled", false);
                    $('#progressbox1').hide();

                    $("#imgAdminImage").attr('src', domainAddress + imageUrl1);
                    $(".fileupload-preview1").text(domainAddress + imageUrl1);
                    $scope.errorUserProfileImage = '';
                    $(".labelProfileImage").css('color','#514d6a');
                    $("#imgAdminImage").css('border', '1px solid grey');
                    $('.errorMessage').text('');
                }
            },
            error: function() {

            }
        }).submit();
    }); //ImageUpload     

    $scope.onPhoneNumberChange = function(value) {
        if(value!=undefined && value.length<10){
            $(".labelPhoneNumber").css('color','red');
            $("#UserPhoneNumber").css('border-color','red');
            $('.errorMessage').text('* Phone Number should be 10 digit');
            return false;
        } else {
            $(".labelPhoneNumber").css('color','#514d6a');
            $("#UserPhoneNumber").css('border-color','#dfe4ed');
            $('.errorMessage').text('');
        }
    }
 

    $scope.updateProfile = function() {
        if(imageUrl1==''){
            $scope.errorUserProfileImage = '* Select the Profile Image to Upload';
            return false;
        }  else if($scope.UserName==undefined && $scope.UserEmailID==undefined && $scope.UserPhoneNumber==undefined && $scope.UserAddress==undefined) {
            $scope.errorUserName = '* Enter the Name';
            $scope.errorUserPhoneNumber = '* Enter the Phone Number';
            $scope.errorUserEmailID = '* Enter the EmailID';
            $scope.errorUserAddress = '* Enter the Address';
            return false;
        } else if($scope.UserName==undefined){
            $scope.errorUserName = '* Enter the Name';
            return false;
        } else if($scope.UserPhoneNumber==undefined){
            $scope.errorUserPhoneNumber = '* Enter the Phone Number';
            return false;
        } else if($scope.UserEmailID==undefined){
            $scope.errorUserEmailID = '* Enter the EmailID';
            return false;
        } else if($scope.UserAddress==undefined){
            $scope.errorUserAddress = '* Enter the Address';
            return false;
        } else if($scope.UserApplicationAmount==undefined){
            $scope.errorUserApplicationAmount = '* Enter the Application Amount';
            return false;
        } else {
            var userdata =
            '{"UserName":"' + $scope.UserName + '","UserPhoneNumber":"' + $scope.UserPhoneNumber + '","UserEmailID":"' + $scope.UserEmailID + '","UserAddress":"' + encodeURIComponent($scope.UserAddress.replace(/'/g, "′")) + '","UserPhoto":"' + imageUrl1 + '","UserApplicationAmount":"'+$scope.UserApplicationAmount+'","UserType":"'+getMfiLoginData[0].UserType+'","UserCompanyID":"'+getMfiLoginData[0].Company[0].CompanyID+'"}';
            var req = {};

            if ($scope.userID != 0) {
                req = {
                    method: 'POST',
                    url: domainAddress + 'UpdateUserProfile/' + $scope.userID,
                    data: userdata
                }
            }
            $http(req).then(function(resp) {
                if (resp.data.status == 'Success') {
                    if (resp.data.isExistUser == 0) {
                        $("#UserProfilepopup").modal('show');
                        
                    } else {
                        $("#UserProfilepopupexit").modal('show');
                    
                    }
                    getMfiLoginData[0].UserName = $scope.UserName;
                    getMfiLoginData[0].UserPhoneNumber = $scope.UserPhoneNumber;
                    getMfiLoginData[0].UserEmailID = $scope.UserEmailID;
                    getMfiLoginData[0].UserAddress = decodeURIComponent($scope.UserAddress);
                    getMfiLoginData[0].UserPhoto = imageUrl1;
                    if(getMfiLoginData[0].UserType == 'Admin'){
                        getMfiLoginData[0].UserApplicationAmount = $scope.UserApplicationAmount;
                    }
                    localStorage.setItem("mfiInfo", JSON.stringify(getMfiLoginData));
                    if (imageUrl1 == "" || imageUrl1 == null) {
                        $("#imgAdminImage").attr("src", "../assets/common/img/noImage.gif");
                        imageUrl1 = "";
                        $rootScope.companyLogo = "../assets/common/img/noImage.gif";
                    } else {
                        $("#imgAdminImage").attr("src", domainAddress + imageUrl1);
                        $rootScope.companyLogo = domainAddress + imageUrl1;
                    }

                }
            }, function(err) {
                $("#UserProfilepopup").modal('show');
                $(".modelText").text('Something went worng. Please try after sometime.');
            });
        }
    }

    $scope.btnOk = function() {
        window.location.reload();
    };
});