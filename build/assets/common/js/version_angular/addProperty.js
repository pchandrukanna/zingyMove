app.controller('addPropertyController', function($timeout, $http, $rootScope, $window, $scope, $sce) {
    var _latitude = 13.055125;
    var _longitude = 80.222121;
    var element = "map-submit";
    simpleMap(_latitude,_longitude, element, true);
});

app.controller('FileUploadCtrl', function($scope) {
    //============== DRAG & DROP =============
    // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
    $scope.userID = '';

    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
    }

    var dropbox = document.getElementById("dropbox")
    $scope.dropText = 'Drop Property Images here...'

    // init event handlers
    function dragEnterLeave(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
    }
    dropbox.addEventListener("dragenter", dragEnterLeave, false)
    dropbox.addEventListener("dragleave", dragEnterLeave, false)
    dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        var clazz = 'not-available'
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        $scope.$apply(function(){
            $scope.dropText = ok ? 'Drop Property Images here...' : 'Only files are allowed!'
            $scope.dropClass = ok ? 'over' : 'not-available'
        })
    }, false)
    dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
        var files = evt.dataTransfer.files
        if (files.length > 0) {
            $scope.$apply(function(){
                $scope.files = []
                for (var i = 0; i < files.length; i++) {
                    $scope.files.push(files[i])
                }
            })
        }
    }, false)
    //============== DRAG & DROP =============

    $scope.setFiles = function(element) {
        // console.log(element.value.match(/\.(.+)$/)[1])
        console.log("test");
        console.log(element.value.match(/\.(.+)$/)[1]);
        // for(var m = 0; m < element.length; ) {
        //     console.log(element.value.match(/\.(.+)$/)[1])
        // }
        // var ext = this.value.match(/\.(.+)$/)[1];
        // switch (ext) {
        //     case 'jpg':
        //     case 'jpeg':
        //     case 'png':
        //     case 'gif':
        //         $('#uploadButton').attr('disabled', false);
        //         break;
        //     default:
        //         alert('This is not an allowed file type.');
        //         this.value = '';
        // }

        
        // $scope.$apply(function($scope) {
        //     console.log('files:', element.files);
        //     // Turn the FileList object into an Array
        //         $scope.files = []
        //         for (var i = 0; i < element.files.length; i++) {
        //         $scope.files.push(element.files[i])
        //         }
        //     $scope.progressVisible = false
        // });
    };

    $scope.uploadFile = function() {
        var fd = new FormData()
        for (var i in $scope.files) {
            fd.append("uploadedFile[]", $scope.files[i])
        }
        fd.append("userID",$scope.userID)
        // console.log($scope.files[0]);
        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        xhr.addEventListener("error", uploadFailed, false)
        xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", domainAddress+"UploadPropertyImage")
        $scope.progressVisible = true
        xhr.send(fd)
    }

    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                $scope.progress = 'unable to compute'
            }
        })
    }

    function uploadComplete(evt) {
        alert(evt.target.responseText)
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }

    function uploadCanceled(evt) {
        $scope.$apply(function(){
            $scope.progressVisible = false
        })
        alert("The upload has been canceled by the user or the browser dropped the connection.")
    }
});