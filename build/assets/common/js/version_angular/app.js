'use strict';

var domainAddress = "http://localhost:8888/zingyMoveAPI/";
var domainAddressImage = "http://localhost:8888/zingyMoveAPI/";
// var misDomainAddress = "http://localhost:8888/info-g/";

// var domainAddress = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/dev.mfiapi/";
// var domainAddressImage  = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/dev.mfiapi/ajaximage.php";
// var misDomainAddress = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/info-g/";

// var domainAddress = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/mfiapi/";
// var domainAddressImage  = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/mfiapi/ajaximage.php";
// var misDomainAddress = "http://ec2-13-126-220-104.ap-south-1.compute.amazonaws.com/info-g/";

angular.module('cleanUI', [
    'ngRoute',
    'cleanUI.controllers',
])
.config(['$locationProvider', '$routeProvider',
    function($locationProvider, $routeProvider) {

        /////////////////////////////////////////////////////////////
        // SYSTEM
        $routeProvider.when('/', {redirectTo: '/Landing'});
        $routeProvider.otherwise({redirectTo: 'pages/page-404'});

      

        $routeProvider.when('/DashboardRenter', {
            templateUrl: 'dashboards/dashboardRenter.html',
            controller: 'renterDashboardController'
        });
        $routeProvider.when('/DashboardOwner', {
            templateUrl: 'dashboards/dashboardOwner.html',
            controller: 'ownerDashboardController'
        });
        $routeProvider.when('/Landing', {
            templateUrl: 'dashboards/landing.html',
            controller: 'adminDashboardController'
        });

        $routeProvider.when('/AddProperty', {
            templateUrl: 'pages/addProperty.html',
            controller: 'addPropertyController'
        });

        $routeProvider.when('/ListProperty', {
            templateUrl: 'pages/listProperty.html',
            controller: 'listPropertyController'
        });

        $routeProvider.when('/PropertyDetails', {
            templateUrl: 'pages/propertyDetails.html',
            controller: 'propertyDetailsController'
        });

        // $routeProvider.when('/Login', {
        //     templateUrl: 'pages/login-beta.html',
        //     controller: 'loginPageCtrl'
        // });

        $routeProvider.when('/pages/page-404', {
            templateUrl: 'pages/page-404.html'
        });

        $routeProvider.when('/pages/page-500', {
            templateUrl: 'pages/page-500.html'
        });

       
    }
]);

var app = angular.module('cleanUI.controllers', []);

app.controller('MainCtrl', function($location, $scope, $rootScope, $timeout, $http) {

    NProgress.configure({
        minimum: 0.2,
        trickleRate: 0.1,
        trickleSpeed: 200
    });

    $scope.$on('$routeChangeStart', function() {

        // NProgress Start
        $('body').addClass('cui-page-loading-state');
        NProgress.start();

    });

    $scope.$on('$routeChangeSuccess', function() {

        // Set to default (show) state left and top menu, remove single page classes
        $('body').removeClass('single-page single-page-inverse');
        $rootScope.hideLeftMenu = false;
        $rootScope.hideTopMenu = false;
        $rootScope.showFooter = true;

        // Firefox issue: scroll top when page load
        $('html, body').scrollTop(0);

        // Set active state menu after success change route
        $('.left-menu-list-active').removeClass('left-menu-list-active');
        $('nav.left-menu .left-menu-list-root .left-menu-link').each(function(){
            if ($(this).attr('href') == '#' + $location.path()) {
                $(this).closest('.left-menu-list-root > li').addClass('left-menu-list-active');
            }
        });

        // NProgress End
        setTimeout(function(){
            NProgress.done();
        }, 1000);
        $('body').removeClass('cui-page-loading-state');
    });
});

