app.controller('adminDashboardController', function($timeout, $http, $rootScope, $window, $scope, $sce) {

    $scope.isSubmitted = false;
    $scope.userTitle = 'Mr.';
    $scope.userType = 'Owner';
    
    $scope.onPhoneChange = function(mobileNumber) {
        if(!isNaN(mobileNumber) && mobileNumber.length > 9) {
            $scope.errorMobileNumber = false;
        } else {
            $scope.errorMobileNumber = true;
        }
    }
    $scope.onPasswordChange = function(password) {
        if(password.length > 5) {
            $scope.errorPasswordLength = false;
        } else {
            $scope.errorPasswordLength = true;
        }
    }
    $scope.onConfirmPassChange = function(confirm_password) {
        if(confirm_password.length > 5 && (confirm_password == $scope.password)) {
            $scope.errorConfirmPass = false;
        } else {
            $scope.errorConfirmPass = true;
        }
    }
    $scope.onRegister= function() {
        $scope.isSubmitted = true;

        if(isNaN($scope.mobileNumber)) {
            $scope.errorMobileNumber = true;
        } else if($scope.mobileNumber.length != 10) {
            $scope.errorMobileNumber = true;
        } else if($scope.password.length < 6) {
            $scope.errorPasswordLength = true;
        } else if($scope.password != $scope.confirm_password) {
            $scope.errorConfirmPass = true;
        } else {  
            var postdata = '{"userTitle":"'+$scope.userTitle+'","userName":"'+$scope.userName+'","userEmail": "'+$scope.email+'","userPhone": "'+$scope.mobileNumber+'","userPassword": "'+$scope.password+'","userType": "'+$scope.userType+'"}';
            
            console.log(postdata);
            var req = {
                method: 'POST',
                url: domainAddress+'CreateUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                $scope.spinnerload='';
                $scope.alreadyExist = false;
                var response = resp.data;
                if(response.status === "Success"){
                    $('#modal-register').modal('toggle');
                    localStorage.setItem("zingyMoveUserInfo", JSON.stringify(response.records));
                        
                    var getZingyMoveLoginData = response.records;
                    console.log(JSON.stringify(getZingyMoveLoginData));
                    $rootScope.$emit("UserAccessControlMethod", {});
                    if(getZingyMoveLoginData[0].userType == 'Owner'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardOwner";
                        window.location.reload();
                    }
                    else if(getZingyMoveLoginData[0].userType == 'Renter'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    } else {
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    }
                    
                } else {
                    console.log(response.records);
                }
            });
        }
    }

    $scope.onLoginMailChange = function(loginMail) {
        $scope.loginFailed = false;
        if($scope.loginForm.loginMail.$valid) {
            $scope.errorLoginMail = false;
        }
    }
    
    $scope.onLoginPassChange = function(loginPass) {
        $scope.loginFailed = false;
        if(loginPass == '') {
            $scope.errorLoginPass = true;
        } else {
            $scope.errorLoginPass = false;
        }
    }

    $scope.onLogin = function() {
        $scope.isSubmitted = true;
        if($scope.loginForm.loginMail.$invalid) {
            $scope.errorLoginMail = true;
        } else if($scope.loginPass == '' || $scope.loginPass == undefined) {
            $scope.errorLoginPass = true;
        } else {
            var postdata = '{"userPhone":"","userPassword":"'+$scope.loginPass+'","userEmail":"'+$scope.loginMail+'"}';
            
            console.log(postdata);
            var req = {
                method: 'POST',
                url: domainAddress+'LoginUser',
                data: postdata
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    $('#modal-sign-in').modal('toggle');
                    $('.modal-backdrop').removeClass('modal-backdrop');
                    localStorage.setItem("zingyMoveUserInfo", JSON.stringify(response.records));
                        
                    var getZingyMoveLoginData = response.records;
                    console.log(JSON.stringify(getZingyMoveLoginData));
                    
                    $rootScope.$emit("UserAccessControlMethod", {});
                    if(getZingyMoveLoginData[0].userType == 'Owner'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardOwner";
                        window.location.reload();
                    }
                    else if(getZingyMoveLoginData[0].userType == 'Renter'){
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    } else {
                        $rootScope.$emit("CallTopMenuMethod", {});
                        window.location.href="#!/DashboardRenter";
                        window.location.reload();
                    }
                } else if(response.status === "failure"){
                    $scope.loginFailed = true;
                    $scope.loginMail = '';
                    $scope.loginPass = '';
                    $scope.errorLoginMail = false;
                    $scope.errorLoginPass = false;
                    // $('#modal-sign-in').modal('toggle');
                } else {
                    console.log("Error in  login");
                }
            });
        }
    }

        var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
        if(getZingyMoveUserData!=null){

            $rootScope.$emit("UserAccessControlMethod", {});
            if(getZingyMoveUserData[0].userType == 'Owner'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardOwner";
                window.location.reload();
            }
            else if(getZingyMoveUserData[0].userType == 'Renter'){
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardRenter";
                window.location.reload();
            } else {
                $rootScope.$emit("CallTopMenuMethod", {});
                window.location.href="#!/DashboardRenter";
                window.location.reload();
            }
        } 
});