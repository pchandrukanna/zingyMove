app.controller('sideMenuController', function($timeout, $http, $rootScope, $window, $scope) {
  var isProductClick = false;
  var isInvestorClick = false;
  var userID = 0;
  var companyID = 0;
  var userType = '';
  var countToggle = 0;
  var countPrecloserToggle = 0;
  
  localstorageactivemenu();

  $rootScope.$on("UserAccessControlMethod", function(){
    localstorageactivemenu();
  });

  function localstorageactivemenu(){
    $rootScope.isCompany = false;
    $rootScope.isusercontrolpage = false;
    $rootScope.isMaster= false;
    $rootScope.isArea= false;
    $rootScope.isBranch= false;
    $rootScope.isStaff= false;
    $rootScope.isMember= false;
    $rootScope.isApplicationAmount= false;
    $rootScope.isGroup= false;
    $rootScope.isProduct= false;
    $rootScope.isFunder= false;
    $rootScope.isThrift= false;
    $rootScope.isOperations= false;
    $rootScope.isFundDetails= false;
    $rootScope.isLoanDisbursement= false;
    $rootScope.isTodayDemandcollection= false;
    $rootScope.isGroupCollection= false;
    $rootScope.isTodayReport= false;
    $rootScope.isMapThriftmember= false;
    $rootScope.isThriftCollection= false;
    $rootScope.isPreCloseLoan= false;
    $rootScope.isMemberCollection= false;
    $rootScope.isGroupwiseMemberCollection= false;
    $rootScope.isChangeGroupOfficer= false;
    $rootScope.isReports= false;
    $rootScope.isOLBBasedReports= false;
    $rootScope.isProductReport= false;
    $rootScope.isActivityReport= false;
    $rootScope.isGroupReport= false;
    $rootScope.isBranchReport= false;
    $rootScope.isMemberDetailsReport= false;
    $rootScope.isGroupOfficerReport= false;
    $rootScope.isPreCloseReports= false;
    $rootScope.isGroupBasedPreClose= false;
    $rootScope.isMemberBasedPreClose= false;
    $rootScope.isTodayCollections= false;
    $rootScope.isDayWiseCollections= false;
    $rootScope.isDayWiseGroupCollections= false;
    $rootScope.isActiveMembers= false;
    $rootScope.isInactiveMembers= false;
    $rootScope.isClosedLoanDetails= false;
    $rootScope.isBranchAssessments= false;
    $rootScope.isOverdueReports= false;
    $rootScope.isFunderReports= false;
    $rootScope.isRepaymentCollection= false;
    $rootScope.isExpectedDemand= false;
    $rootScope.isDemandCollection= false;
    $rootScope.isLoanDisbursementreport= false;
    $rootScope.isFinance= false;
    $rootScope.isMastersreport= false;
    $rootScope.isTransactionsReport= false;
    $rootScope.isFinanceReport= false;
    $rootScope.isLedgers= false;
    $rootScope.isGroup= false;
    $rootScope.isGroupAccount= false;
    $rootScope.isGstType= false;
    $rootScope.isParty= false;
    $rootScope.isInstruments= false;
    $rootScope.isMiscellaneous= false;
    $rootScope.isAccount= false;
    $rootScope.isBillComponentMaster= false;
    $rootScope.isPurchaseEntry= false;
    $rootScope.isReceipts= false;
    $rootScope.isPayment= false;
    $rootScope.isContra= false;
    $rootScope.isJournals= false;
    $rootScope.isLedgerReport= false;
    $rootScope.isIncomeAndExpenditure= false;
    $rootScope.isProfitLoss= false;
    $rootScope.isReceiptsPayments= false;
    $rootScope.isTrialBalance= false;
    $rootScope.isBalanceSheet= false;
    $rootScope.isSchedulesToBalanceSheet= false;
    $rootScope.isFunderPage = false;
    $rootScope.isEconomicActivity= false;
    $rootScope.isPurposeOfLoan= false;
    $rootScope.isSendNotification= false;
    $rootScope.isThriftReport= false;
    $rootScope.isLoanCard= false;
    $rootScope.isInsurence= false;

      var getMfiLoginData = JSON.parse(localStorage.getItem("mfiInfo"));
      if (getMfiLoginData != null) {
        userID = getMfiLoginData[0].UserID;
        userType = getMfiLoginData[0].UserType;
        companyID = getMfiLoginData[0].Company[0].CompanyID;
        if (userType == 'SuperAdmin') {
              $rootScope.isCompany = true;
          }
       else if (userType == 'Admin') {
            $rootScope.isusercontrolpage = true;
            $rootScope.isMaster= true;
            $rootScope.ismapmenu = true;
            $rootScope.isCustommapname =true;
            $rootScope.isArea= true;
            $rootScope.isBranch= true;
            $rootScope.isStaff= true;
            $rootScope.isMember= true;
            $rootScope.isApplicationAmount= true;
            $rootScope.isGroup= true;
            $rootScope.isProduct= true;
            $rootScope.isFunder= true;
            $rootScope.isThrift= true;
            $rootScope.isOperations= true;
            $rootScope.isFundDetails= true;
            $rootScope.isLoanDisbursement= true;
            $rootScope.isBonusLoanDisbursement = true;
            $rootScope.isTodayDemandcollection= true;
            $rootScope.isGroupCollection= true;
            $rootScope.isTodayReport= true;
            $rootScope.isThriftCollection= true;
            $rootScope.isPreCloseLoan= true;
            $rootScope.isMemberCollection= true;
            $rootScope.isGroupwiseMemberCollection= true;
            $rootScope.isChangeGroupOfficer= true;
            $rootScope.isReports= true;
            $rootScope.isOLBBasedReports= true;
            $rootScope.isProductReport= true;
            $rootScope.isActivityReport= true;
            $rootScope.isGroupReport= true;
            $rootScope.isBranchReport= true;
            $rootScope.isMemberDetailsReport= true;
            $rootScope.isGroupOfficerReport= true;
            $rootScope.isPreCloseReports= true;
            $rootScope.isGroupBasedPreClose= true;
            $rootScope.isMemberBasedPreClose= true;
            $rootScope.isTodayCollections= true;
            $rootScope.isDayWiseCollections= true;
            $rootScope.isDayWiseGroupCollections= true;
            $rootScope.isActiveMembers= true;
            $rootScope.isInactiveMembers= true;
            $rootScope.isClosedLoanDetails= true;
            $rootScope.isBranchAssessments= true;
            $rootScope.isOverdueReports= true;
            $rootScope.isFunderReports= true;
            $rootScope.isRepaymentCollection= true;
            $rootScope.isExpectedDemand= true;
            $rootScope.isDemandCollection= true;
            $rootScope.isLoanDisbursementreport= true;
            $rootScope.isFinance= true;
            $rootScope.isMastersreport= true;
            $rootScope.isTransactionsReport= true;
            $rootScope.isFinanceReport = true;
            $rootScope.isLedgers= true;
            $rootScope.isGroup= true;
            $rootScope.isGroupAccount= true;
            $rootScope.isGstType= true;
            $rootScope.isParty= true;
            $rootScope.isInstruments=true;
            $rootScope.isAccount= true;
            $rootScope.isBillComponentMaster= true;
            $rootScope.isPurchaseEntry= true;
            $rootScope.isReceipts= true;
            $rootScope.isPayment= true;
            $rootScope.isContra= true;
            $rootScope.isJournals= true;
            $rootScope.isLedgerReport=true;
            $rootScope.isIncomeAndExpenditure= true;
            $rootScope.isProfitLoss= true;
            $rootScope.isReceiptsPayments=true;
            $rootScope.isTrialBalance= true;
            $rootScope.isBalanceSheet= true;
            $rootScope.isSchedulesToBalanceSheet= true;
            $rootScope.isEconomicActivity= true;
            $rootScope.isPurposeOfLoan= true;
            $rootScope.isSendNotification= true;
            $rootScope.isThriftReport= true;
            $rootScope.isLoanCard= true;
            $rootScope.isInsurence= true;
            $rootScope.isdaywiseCollection= true;
            $rootScope.isMapThriftmember= true;
            $rootScope.isOfficerInsentive= true;
            $rootScope.isChangeMeetingDate= true;
            $rootScope.isDaywiseCollectionname = "Day Wise Collection";
            $rootScope.isDayWiseGroupCollectionname = "Day Wise Group Collection";
            $rootScope.isAreaname = "Area";        
            $rootScope.isBranchname = "Branch/Center";
            $rootScope.isStaffname = "Staff";
            $rootScope.isMembername = "Member";
            $rootScope.isApplicationAmountName = "Application Amount";
            $rootScope.isThriftname = "Thrift";
            $rootScope.isGroupname = "Group";
            $rootScope.isProductname = "Product";
            $rootScope.isFundername = "Funder";
            $rootScope.isEconomicActivityname = "Economic Activity";
            $rootScope.isPurposeOfLoanName = "Purpose Of Loan";
            $rootScope.isSendnotificationname = "Send Notificationname";
            $rootScope.isFunderDetailsname = "Funder Details";
            $rootScope.isLoanDisbursementname = "Loan Disbursement";
            $rootScope.isTopUpLoanname = "TopUp Loan";
            $rootScope.isDemandCollectionname = "Demand Collection";
            $rootScope.isGroupCollectionname = "Group Collection";
            $rootScope.isTodayReportname = "Today Collection Status";
            $rootScope.isThriftCollectionname = "Thrift Collection";
            $rootScope.isPrecloseLoanname = "Pre-close Loan";
            $rootScope.isMemberCollectionname = "Member Collection";
            $rootScope.isGroupwisemembercollectionname = "Group wise member collection";
            $rootScope.isChangeGroupOfficername = "Change Group Officer";
            $rootScope.isOLBBasedReportsname = "OLB Based Reports";
            $rootScope.isProductReportname = "Product Report";
            $rootScope.isActivityReportname = "Activity Report";
            $rootScope.isGroupReportname = "Group Report";
            $rootScope.isMemberDetailsreportname = "Member Details report";
            $rootScope.isBranchReportname = "Branch Report";
            $rootScope.isGroupofficerReportname = "Group Officer Report";
            $rootScope.isPreCloseReportname = "Pre Close Report";
            $rootScope.isGroupbasedpreclosename = "Group Based Pre-Close";
            $rootScope.isMemberbasedpreclosename = "Member Based Pre-Close";
            $rootScope.isTodayCollectionname = "Today Collection";
            $rootScope.isDayWiseCollectionname = "DayWise Collection";
            $rootScope.isActiveMembername = "Active Member";
            $rootScope.isInsurancename = "Insurance";
            $rootScope.isInactiveMembername = "Inactive Member";
            $rootScope.isClosedLoandetailsname = "Closed Loan details";
            $rootScope.isBranchAssessmentname = "Branch Assessment";
            $rootScope.isOverdueReportname = "Overdue Report";
            $rootScope.isFunderReportname = "Funder Report";
            $rootScope.isRepaymentCollectionname = "Repayment Collection";
            $rootScope.isUpcomingdemandname = "Upcoming Demand";
            $rootScope.isDemandCollectionReportname = "Demand Collection Report";
            $rootScope.isLoanDisbursementCollectionname = "Loan Disbursement";
            $rootScope.isLoanCardname = "Loan Card";
            $rootScope.isThriftReportsname = "Thrift Report";
            $rootScope.isOfficerInsentivename = "Officer Incentive";
            $rootScope.isChangeMeetingDatename = "Change Meeting Date";
            $rootScope.isMapThriftmembername = "Map Thrift to Member";
        }
        else{
          $rootScope.isusercontrolpage = false;
          $rootScope.isMaster= false;
          $rootScope.ismapmenu = false;
          $rootScope.isFinance= false;
          

        }
        
        $http({
          method: 'GET',
          url: domainAddress+'GetAllusercontroldetails/'+companyID+'/'+userType,
        }).then(function successCallback(response) {
          $scope.isLoading = false;
          if(response.data.record_count == 0) {
            $scope.noAreaList = true;
            $scope.listAllMenulist = '';
          } else {
            $scope.listAllMenulist = response.data.records;
            
          }
          usercontrolaccesssfunction(userType);
        }, function(err) {
          $("#areaModal").modal('show');
          $(".modelText").text('Something went wrong. Please try after sometime.');
        });
      }  
      function usercontrolaccesssfunction(userType){
        for (var menulist in $scope.listAllMenulist) {
          
        for (var submenulist in $scope.listAllMenulist[menulist].submenu) {
          if($scope.listAllMenulist[menulist].userType == userType){
           
            if($scope.listAllMenulist[menulist].menuName   == 'Master'){
              $rootScope.isMaster= true;
            }
            if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){
              if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Area'){
                if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){
  
                }
                $rootScope.isArea= true;
                if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                  $rootScope.isAreaname = "Area";
                }else{
                  $rootScope.isAreaname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
                }
              }
            }
            if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Branch'){
              $rootScope.isBranch= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isBranchname = "Branch/Center";
              }else{
                $rootScope.isBranchname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Staff'){
              $rootScope.isStaff= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isStaffname = "Staff";
              }else{
                $rootScope.isStaffname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Member'){
              $rootScope.isMember= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isMembername = "Member";
              }else{
                $rootScope.isMembername = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Application Amount'){
              $rootScope.isApplicationAmount= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isApplicationAmountName = "Application Amount";
              }else{
                $rootScope.isApplicationAmountName = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Thrift'){
              $rootScope.isThrift= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isThriftname = "Thrift";
              }else{
                $rootScope.isThriftname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group'){
              $rootScope.isGroup= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupname = "Group";
              }else{
                $rootScope.isGroupname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Product'){
              $rootScope.isProduct= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isProductname = "Product";
              }else{
                $rootScope.isProductname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Funder'){
              $rootScope.isFunder= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isFundername = "Funder";
              }else{
                $rootScope.isFundername = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Economic Activity'){
              $rootScope.isEconomicActivity= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isEconomicActivityname = "Economic Activity";
              }else{
                $rootScope.isEconomicActivityname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
           if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Purpose of Loan'){
              $rootScope.isPurposeOfLoan= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isPurposeOfLoanName = "Purpose Of Loan";
              }else{
                $rootScope.isPurposeOfLoanName = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
         if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Send Notification'){
              $rootScope.activeSendnotification= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.activeSendnotificationname = "Send Notification";
              }else{
                $rootScope.activeSendnotificationname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
            if($scope.listAllMenulist[menulist].menuName   == 'Operations'){
              $rootScope.isOperations= true;
            }
            if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Funder Details'){
              $rootScope.isFundDetails= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isFunderDetailsname = "Funder Details";
              }else{
                $rootScope.isFunderDetailsname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Day Wise Collection'){
              $rootScope.isFundDetails= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isFunderDetailsname = "Day Wise Collection";
              }else{
                $rootScope.isFunderDetailsname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Loan Disbursement'){
              $rootScope.isLoanDisbursement= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isLoanDisbursementname = "Loan Disbursement";
              }else{
                $rootScope.isLoanDisbursementname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'TopUp Loan'){
              $rootScope.isBonusLoanDisbursement= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isTopUpLoanname = "TopUp Loan";
              }else{
                $rootScope.isTopUpLoanname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Demand Collection'){
              $rootScope.isTodayDemandcollection= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isDemandCollectionname = "Demand Collection";
              }else{
                $rootScope.isDemandCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Today Collection Status'){
              $rootScope.isGroupCollection= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isTodayReportname = "Today Collection Status";
              }else{
                $rootScope.isTodayReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group Collection'){
              $rootScope.isGroupCollection= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupCollectionname = "Group Collection";
              }else{
                $rootScope.isGroupCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Map Thrift to Member'){
              $rootScope.isMapThriftmember= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isMapThriftmembername = "Map Thrift to Member";
              }else{
                $rootScope.isMapThriftmembername = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Thrift Collection'){
              $rootScope.isThriftCollection= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isThriftCollectionname = "Thrift Collection";
              }else{
                $rootScope.isThriftCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Pre-close Loan'){
              $rootScope.isPreCloseLoan= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isPrecloseLoanname = "Pre-close Loan";
              }else{
                $rootScope.isPrecloseLoanname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Member Collection'){
              $rootScope.isMemberCollection= true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isMemberCollectionname = "Member Collection";
              }else{
                $rootScope.isMemberCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group wise member collection'){
              $rootScope.isGroupwiseMemberCollection = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupwisemembercollectionname = "Group wise member collection";
              }else{
                $rootScope.isGroupwisemembercollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Change Group Officer'){
              $rootScope.isChangeGroupOfficer = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isChangeGroupOfficername = "Change Group Officer";
              }else{
                $rootScope.isGroupwisemembercollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Change Meeting Date'){
              $rootScope.isChangeMeetingDate = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isChangeMeetingDatename = "Change Meeting Date";
              }else{
                $rootScope.isGroupwisemembercollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Officer Incentive'){
              $rootScope.isOfficerInsentive = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isOfficerInsentivename = "Officer Incentive";
              }else{
                $rootScope.isGroupwisemembercollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
            if($scope.listAllMenulist[menulist].menuName   == 'Reports'){
              $rootScope.isReports= true;
            }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'OLB Based Reports'){
              $rootScope.isOLBBasedReports = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isOLBBasedReportsname = "OLB Based Reports";
              }else{
                $rootScope.isOLBBasedReportsname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Product Report'){
              $rootScope.isProductReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isProductReportname = "Product Report";
              }else{
                $rootScope.isProductReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Activity Report'){
              $rootScope.isActivityReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isActivityReportname = "Activity Report";
              }else{
                $rootScope.isActivityReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group Report'){
              $rootScope.isGroupReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupReportname = "Group Report";
              }else{
                $rootScope.isGroupReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Member Details report'){
              $rootScope.isMemberDetailsReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isMemberDetailsreportname = "Member Details report";
              }else{
                $rootScope.isMemberDetailsreportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Branch Report'){
              $rootScope.isBranchReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isBranchReportname = "Branch Report";
              }else{
                $rootScope.isBranchReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group Officer Report'){
              $rootScope.isGroupOfficerReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupofficerReportname = "Group Officer Report";
              }else{
                $rootScope.isGroupofficerReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Pre Close Report'){
              $rootScope.isPreCloseReports = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isPreCloseReportname = "Pre Close Report";
              }else{
                $rootScope.isPreCloseReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Group Based Pre-Close'){
              $rootScope.isGroupBasedPreClose = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isGroupbasedpreclosename = "Group Based Pre-Close";
              }else{
                $rootScope.isGroupbasedpreclosename = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Member Based Pre-Close'){
              $rootScope.isMemberBasedPreClose = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isMemberbasedpreclosename = "Member Based Pre-Close";
              }else{
                $rootScope.isMemberbasedpreclosename = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Today Collection'){
              $rootScope.isTodayCollections = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isTodayCollectionname = "Today Collection";
              }else{
                $rootScope.isTodayCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'DayWise Collection'){
              $rootScope.isDayWiseCollections = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isDayWiseCollectionname = "DayWise Collection";
              }else{
                $rootScope.isDayWiseCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'DayWise Group Collection'){
              $rootScope.isDayWiseGroupCollections = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isDayWiseGroupCollectionname = "DayWise Group Collection";
              }else{
                $rootScope.isDayWiseGroupCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Active Member'){
              $rootScope.isActiveMembers = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isActiveMembername = "Active Member";
              }else{
                $rootScope.isActiveMembername = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Insurance'){
              $rootScope.isInsurence = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isInsurancename = "Insurance";
              }else{
                $rootScope.isInsurancename = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Inactive Member'){
              $rootScope.isInactiveMembers = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isInactiveMembername = "Inactive Member";
              }else{
                $rootScope.isInactiveMembername = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Closed Loan details'){
              $rootScope.isClosedLoanDetails = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isClosedLoandetailsname = "Closed Loan details";
              }else{
                $rootScope.isClosedLoandetailsname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Branch Assessment'){
              $rootScope.isBranchAssessments = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isBranchAssessmentname = "Branch Assessment";
              }else{
                $rootScope.isBranchAssessmentname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Overdue Report'){
              $rootScope.isOverdueReports = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isOverdueReportname = "Overdue Report";
              }else{
                $rootScope.isOverdueReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Funder Report'){
              $rootScope.isFunderReports = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isFunderReportname = "Funder Report";
              }else{
                $rootScope.isFunderReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Repayment Collection'){
              $rootScope.isRepaymentCollection = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isRepaymentCollectionname = "Repayment Collection";
              }else{
                $rootScope.isRepaymentCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Upcoming Demand'){
              $rootScope.isExpectedDemand = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isUpcomingdemandname = "Upcoming Demand";
              }else{
                $rootScope.isUpcomingdemandname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Demand Collection'){
              $rootScope.isDemandCollection = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isDemandCollectionReportname = "Demand Collection";
              }else{
                $rootScope.isDemandCollectionReportname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Loan Disbursement'){
              $rootScope.isLoanDisbursementreport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isLoanDisbursementCollectionname = "Loan Disbursement";
              }else{
                $rootScope.isLoanDisbursementCollectionname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Loan Card'){
              $rootScope.isLoanCard = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isLoanCardname = "Loan Card";
              }else{
                $rootScope.isLoanCardname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          if($scope.listAllMenulist[menulist].submenu[submenulist].Isshow == 1 ){  
            if($scope.listAllMenulist[menulist].submenu[submenulist].subMenuName == 'Thrift Report'){
              $rootScope.isThriftReport = true;
              if($scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == null || $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName == ''){
                $rootScope.isThriftReportsname = "Thrift Report";
              }else{
                $rootScope.isThriftReportsname = $scope.listAllMenulist[menulist].submenu[submenulist].MenucustomName;
              }
            }
          }
          }
        }
      }
    }         
        // if (userType == 'SuperAdmin') {
        //     $rootScope.isCompany = true;
        // } else if (userType == 'Admin') {
        //     $rootScope.isusercontrolpage = true;
        //     $rootScope.isMaster= true;
        //     $rootScope.ismapmenu = true;
        //     $rootScope.isArea= true;
        //     $rootScope.isBranch= true;
        //     $rootScope.isStaff= true;
        //     $rootScope.isMember= true;
        //     $rootScope.isGroup= true;
        //     $rootScope.isProduct= true;
        //     $rootScope.isFunder= true;
        //     $rootScope.isThrift= true;
        //     $rootScope.isOperations= true;
        //     $rootScope.isFundDetails= true;
        //     $rootScope.isLoanDisbursement= true;
        //     $rootScope.isBonusLoanDisbursement = true;
        //     $rootScope.isTodayDemandcollection= true;
        //     $rootScope.isGroupCollection= true;
        //     $rootScope.isThriftCollection= true;
        //     $rootScope.isPreCloseLoan= true;
        //     $rootScope.isMemberCollection= true;
        //     $rootScope.isGroupwiseMemberCollection= true;
        //     $rootScope.isReports= true;
        //     $rootScope.isOLBBasedReports= true;
        //     $rootScope.isProductReport= true;
        //     $rootScope.isActivityReport= true;
        //     $rootScope.isGroupReport= true;
        //     $rootScope.isBranchReport= true;
        //     $rootScope.isMemberDetailsReport= true;
        //     $rootScope.isGroupOfficerReport= true;
        //     $rootScope.isPreCloseReports= true;
        //     $rootScope.isGroupBasedPreClose= true;
        //     $rootScope.isMemberBasedPreClose= true;
        //     $rootScope.isTodayCollections= true;
        //     $rootScope.isDayWiseCollections= true;
        //     $rootScope.isActiveMembers= true;
        //     $rootScope.isInactiveMembers= true;
        //     $rootScope.isClosedLoanDetails= true;
        //     $rootScope.isBranchAssessments= true;
        //     $rootScope.isOverdueReports= true;
        //     $rootScope.isFunderReports= true;
        //     $rootScope.isRepaymentCollection= true;
        //     $rootScope.isExpectedDemand= true;
        //     $rootScope.isDemandCollection= true;
        //     $rootScope.isLoanDisbursementreport= true;
        //     $rootScope.isFinance= true;
        //     $rootScope.isMastersreport= true;
        //     $rootScope.isTransactionsReport= true;
        //     $rootScope.isLedgers= true;
        //     $rootScope.isGroup= true;
        //     $rootScope.isGroupAccount= true;
        //     $rootScope.isParty= true;
        //     $rootScope.isInstruments= true;
        //     $rootScope.isAccount= true;
        //     $rootScope.isBillComponentMaster= true;
        //     $rootScope.isPurchaseEntry= true;
        //     $rootScope.isReceipts= true;
        //     $rootScope.isPayment= true;
        //     $rootScope.isContra= true;
        //     $rootScope.isJournals= true;
        //     $rootScope.isIncomeAndExpenditure= true;
        //     $rootScope.isProfitLoss= true;
        //     $rootScope.isReceiptsPayments=true;
        //     $rootScope.isTrialBalance= true;
        //     $rootScope.isBalanceSheet= true;
        //     $rootScope.isSchedulesToBalanceSheet= true;
        //     $rootScope.isEconomicActivity= true;
        //     $rootScope.isThriftReport= true;
        //     $rootScope.isLoanCard= true;
        //     $rootScope.isInsurence= true;
        // } else if(userType == 'AreaManager'){
        //     $rootScope.isMaster = true;
        //     $rootScope.isBranch= true;
        //     $rootScope.isMember= true;
        //     $rootScope.isGroup= true;
        //     $rootScope.isProduct= true;
        //     $rootScope.isFunder= true;
        //     $rootScope.isThrift= false;
        //     $rootScope.isOperations= true;
        //     $rootScope.isFundDetails= true;
        //     $rootScope.isLoanDisbursement= true;
        //     $rootScope.isTodayDemandcollection= true;
        //     $rootScope.isGroupCollection= true;
        //     $rootScope.isThriftCollection= true;
        //     $rootScope.isPreCloseLoan = true;
        //     $rootScope.isMemberCollection= true;
        //     $rootScope.isGroupwiseMemberCollection= true;
        //     $rootScope.isReports= true;
        //     $rootScope.isOLBBasedReports= true;
        //     $rootScope.isProductReport= true;
        //     $rootScope.isActivityReport= true;
        //     $rootScope.isGroupReport= true;
        //     $rootScope.isBranchReport= true;
        //     $rootScope.isMemberDetailsReport= true;
        //     $rootScope.isGroupOfficerReport= true;
        //     $rootScope.isPreCloseReports= true;
        //     $rootScope.isGroupBasedPreClose= true;
        //     $rootScope.isMemberBasedPreClose= true;
        //     $rootScope.isTodayCollections= true;
        //     $rootScope.isDayWiseCollections= true;
        //     $rootScope.isActiveMembers= true;
        //     $rootScope.isInactiveMembers= true;
        //     $rootScope.isClosedLoanDetails= true;
        //     $rootScope.isBranchAssessments= true;
        //     $rootScope.isOverdueReports= true;
        //     $rootScope.isFunderReports= true;
        //     $rootScope.isRepaymentCollection= true;
        //     $rootScope.isExpectedDemand= true;
        //     $rootScope.isDemandCollection= true;
        //     $rootScope.isLoanDisbursementreport= true;
        //     $rootScope.isThriftReport= true;
        //     $rootScope.isLoanCard= true;
        // } else if(userType == 'BranchManager'){
        //     $rootScope.isMaster = true;
        //     $rootScope.isMember= true;
        //     $rootScope.isGroup= true;
        //     $rootScope.isProduct= true;
        //     $rootScope.isFunder= true;
        //     $rootScope.isThrift= false;
        //     $rootScope.isOperations= true;
        //     $rootScope.isFundDetails= true;
        //     $rootScope.isLoanDisbursement= true;
        //     $rootScope.isTodayDemandcollection= true;
        //     $rootScope.isGroupCollection= true;
        //     $rootScope.isThriftCollection= true;
        //     $rootScope.isPreCloseLoan = true;
        //     $rootScope.isMemberCollection= true;
        //     $rootScope.isGroupwiseMemberCollection= true;
        //     $rootScope.isReports= true;
        //     $rootScope.isOLBBasedReports= true;
        //     $rootScope.isProductReport= true;
        //     $rootScope.isActivityReport= true;
        //     $rootScope.isGroupReport= true;
        //     $rootScope.isBranchReport= true;
        //     $rootScope.isMemberDetailsReport= true;
        //     $rootScope.isGroupOfficerReport= true;
        //     $rootScope.isPreCloseReports= true;
        //     $rootScope.isGroupBasedPreClose= true;
        //     $rootScope.isMemberBasedPreClose= true;
        //     $rootScope.isTodayCollections= true;
        //     $rootScope.isDayWiseCollections= true;
        //     $rootScope.isActiveMembers= true;
        //     $rootScope.isInactiveMembers= true;
        //     $rootScope.isClosedLoanDetails= true;
        //     $rootScope.isBranchAssessments= true;
        //     $rootScope.isOverdueReports= true;
        //     $rootScope.isFunderReports= true;
        //     $rootScope.isRepaymentCollection= true;
        //     $rootScope.isExpectedDemand= true;
        //     $rootScope.isDemandCollection= true;
        //     $rootScope.isLoanDisbursementreport= true;
        //     $rootScope.isThriftReport= true;
        //     $rootScope.isLoanCard= true;
        // } else if(userType == 'GroupManager'){
        //     $rootScope.isMaster = true;
        //     $rootScope.isMember= true;
        //     $rootScope.isGroup= true;
        //     $rootScope.isProduct= true;
        //     $rootScope.isFunder= true;
        //     $rootScope.isThrift= false;
        //     $rootScope.isOperations= true;
        //     $rootScope.isFundDetails= true;
        //     $rootScope.isLoanDisbursement= true;
        //     $rootScope.isTodayDemandcollection= true;
        //     $rootScope.isGroupCollection= true;
        //     $rootScope.isThriftCollection= true;
        //     $rootScope.isPreCloseLoan = true;
        //     $rootScope.isMemberCollection= true;
        //     $rootScope.isGroupwiseMemberCollection= true;
        //     $rootScope.isReports= true;
        //     $rootScope.isOLBBasedReports= true;
        //     $rootScope.isProductReport= true;
        //     $rootScope.isActivityReport= true;
        //     $rootScope.isGroupReport= true;
        //     $rootScope.isBranchReport= true;
        //     $rootScope.isMemberDetailsReport= true;
        //     $rootScope.isGroupOfficerReport= true;
        //     $rootScope.isPreCloseReports= true;
        //     $rootScope.isGroupBasedPreClose= true;
        //     $rootScope.isMemberBasedPreClose= true;
        //     $rootScope.isTodayCollections= true;
        //     $rootScope.isDayWiseCollections= true;
        //     $rootScope.isActiveMembers= true;
        //     $rootScope.isInactiveMembers= true;
        //     $rootScope.isClosedLoanDetails= true;
        //     $rootScope.isBranchAssessments= true;
        //     $rootScope.isOverdueReports= true;
        //     $rootScope.isFunderReports= true;
        //     $rootScope.isRepaymentCollection= true;
        //     $rootScope.isExpectedDemand= true;
        //     $rootScope.isDemandCollection= true;
        //     $rootScope.isLoanDisbursementreport= true;
        //     $rootScope.isThriftReport= true;
        //     $rootScope.isLoanCard= true;
        // } else if(userType == 'GroupOfficer'){
        //     $rootScope.isMaster = true;
        //     $rootScope.isMember= true;
        //     $rootScope.isOperations= true;
        //     $rootScope.isGroupCollection= true;
        //     $rootScope.isThriftCollection= true;
        //     $rootScope.isPreCloseLoan = true;
        //     $rootScope.isMemberCollection= true;
        //     $rootScope.isGroupwiseMemberCollection= true;
        //     $rootScope.isOLBBasedReports= true;
        //     $rootScope.isProductReport= true;
        //     $rootScope.isActivityReport= true;
        //     $rootScope.isGroupReport= true;
        //     $rootScope.isBranchReport= true;
        //     $rootScope.isMemberDetailsReport= true;
        //     $rootScope.isGroupOfficerReport= true;
        //     $rootScope.isPreCloseReports= true;
        //     $rootScope.isGroupBasedPreClose= true;
        //     $rootScope.isMemberBasedPreClose= true;
        //     $rootScope.isTodayCollections= true;
        //     $rootScope.isDayWiseCollections= true;
        //     $rootScope.isActiveMembers= true;
        //     $rootScope.isInactiveMembers= true;
        //     $rootScope.isClosedLoanDetails= true;
        //     $rootScope.isBranchAssessments= true;
        //     $rootScope.isOverdueReports= true;
        //     $rootScope.isFunderReports= true;
        //     $rootScope.isRepaymentCollection= true;
        //     $rootScope.isExpectedDemand= true;
        //     $rootScope.isDemandCollection= true;
        //     $rootScope.isLoanDisbursementreport= true;
        //     $rootScope.isThriftReport= true;
        //     $rootScope.isLoanCard= true;
        // } else if(userType == 'Investor'){
        //     $rootScope.isFunderPage = true;
        // }

  }

  var getValue = localStorage.getItem('ActiveMenu');  
  
  getMenuActiveCheck(getValue);
  
  $scope.menuClick = function(getValue) {
    localStorage.setItem('ActiveMenu',getValue);
    getMenuActiveCheck(getValue);
    if(getValue == 'Product'){
      isProductClick = true;
    } else if(getValue == 'Investor'){
      isInvestorClick = true;
    }
  };

  $scope.logoClick = function(){
      var getMfiLoginData = JSON.parse(localStorage.getItem("mfiInfo"));
      if (getMfiLoginData != null) {
        userType = getMfiLoginData[0].UserType;
        if(userType!='Investor'){
          window.location.href = "#!/Dashboard";
        }
      }
  }

  $scope.olbBasedReportsMenu = function(getMenuValue) {
    countToggle++;
    if(countToggle % 2 !=0 && getMenuValue==1) {
      countToggle++
    }

    if(countToggle % 2 ==0 && getMenuValue==1) {
      $scope.isOpenOlbReports = true;
      $scope.isPreCloserReports = true;
      $scope.isFinanceMasters = true;
    } else {
      $scope.isOpenOlbReports = false;
      $scope.isPreCloserReports = false;
      $scope.isFinanceMasters = false;
    }
  }

  function getMenuActiveCheck(getValue) {
    $scope.isOpenOlbReports = false;
    $scope.isPreCloserReports = false;
    $scope.isFinanceMasters = false;

    $scope.activeCompany = false;
    $scope.activeCompanyIcon = false;
    $scope.activeMapMenu = false;
    $scope.activeMapMenuIcon = false;
    $scope.activeisCustommapname = false;
    $scope.activeisCustommapnameIcon = false;
    $scope.activeAssignUserRights = false;
    $scope.activeAssignUserRightsIcon = false;
    $scope.activeArea = false;
    $scope.activeAreaIcon = false;
    $scope.activeBranch = false;
    $scope.activeBranchIcon = false;
    $scope.activeStaff = false;
    $scope.activeStaffIcon = false;
    $scope.activeGroup = false;
    $scope.activeGroupIcon = false;
    $scope.activeMember = false;
    $scope.activeMemberIcon = false;
    $scope.activeApplicationAmount = false;
    $scope.activeApplicationAmountIcon = false;
    $scope.activeProduct = false;
    $scope.activeProductIcon = false;
    $scope.activeFunder = false;
    $scope.activeFunderIcon = false;
    $scope.activeThrift = false;
    $scope.activeThriftIcon = false;
    $scope.activeInsurence = false;
    $scope.activeInsurenceIcon = false;

    $scope.activeFundDetails = false;
    $scope.activeFundDetailsIcon = false;
    $scope.activeLoanDisbursement = false;
    $scope.activeLoanDisbursementIcon = false;
    $scope.activeBonusLoanDisbursement = false;
    $scope.activeBonusLoanDisbursementIcon = false;
    $scope.activeGroupCollection = false;
    $scope.activeGroupCollectionIcon = false;
    $scope.activeTodayReport = false;
    $scope.activeTodayReportIcon = false;
    $scope.activedaywiseCollection = false;
    $scope.activedaywiseCollectionIcon = false;
    $scope.activeThriftCollection = false;
    $scope.activeThriftCollectionIcon = false;
    $scope.activeMapthriftmember = false;
    $scope.activeMapthriftmemberIcon = false;
    $scope.activePreCloseLoan = false;
    $scope.activePreCloseLoanIcon = false;
    $scope.activeMemberCollection = false;
    $scope.activeMemberCollectionIcon = false;
    $scope.activeGroupMemberCollection = false;
    $scope.activeGroupMemberCollectionIcon = false;
    $scope.activeTodayCollections = false;
    $scope.activeTodayCollectionsIcon = false;

    $scope.activeChangeGroupOfficer = false;
    $scope.activeChangeGroupOfficerIcon = false;
    $scope.activeChangeMeetingDate = false;
    $scope.activeChangeMeetingDateIcon = false;
    $scope.activeOfficerinsentive = false;
    $scope.activeOfficerinsentiveIcon = false;

    $scope.activeProductReport = false;
    $scope.activeProductReportIcon = false;
    $scope.activeActivityReport = false;
    $scope.activeActivityReportIcon = false;
    $scope.activeAreaReport = false;
    $scope.activeAreaReportIcon = false;
    $scope.activeBranchReport = false;
    $scope.activeBranchReportIcon = false;
    $scope.activememberDetailsreport = false;
    $scope.activememberDetailsreportIcon = false;
    $scope.activeGroupOfficerReport = false;
    $scope.activeGroupOfficerReportIcon = false;
    $scope.activePreCloserGroupReport = false;
    $scope.activePreCloserGroupReportIcon = false;
    $scope.activePreCloserMemberReport = false;
    $scope.activePreCloserMemberReportIcon = false;
    
    $scope.activeTodayCollectionsReports = false;
    $scope.activeTodayCollectionsReportsIcon = false;
    $scope.activeDayWiseCollectionsReports = false;
    $scope.activeDayWiseCollectionsReportsIcon = false;
    $scope.activeDayWiseGroupCollectionsReport = false;
    $scope.activeDayWiseGroupCollectionsReportIcon = false;
    $scope.activeMembers = false;
    $scope.activeMembersIcon = false;
    $scope.activeInsurance = false;
    $scope.activeInsuranceIcon = false;
    $scope.activeInActiveMember = false;
    $scope.activeInActiveMemberIcon = false;
    $scope.activeClosedLoanDetails = false;
    $scope.activeClosedLoanDetailsIcon = false;
    $scope.activeBranchAssessments = false;
    $scope.activeBranchAssessmentsIcon = false;
    $scope.activeOverdueReports = false;
    $scope.activeOverdueReportsIcon = false;
    $scope.activeFunderReports = false;
    $scope.activeFunderReportsIcon = false;
    $scope.activeRepaymentCollection = false;
    $scope.activeRepaymentCollectionIcon = false;
    $scope.activeExpectedDemand = false;
    $scope.activeExpectedDemandIcon = false;
    $scope.activeDemandCollection = false;
    $scope.activeDemandCollectionIcon = false;
    $scope.activeLoanDisbursementReport = false;
    $scope.activeLoanDisbursementReportIcon = false;
    $scope.activeThriftReport = false;
    $scope.activeThriftReportIcon = false;
    $rootScope.activeLoanCard = false;
    $rootScope.activeLoanCardIcon = false;

    $scope.activeLedgers = false;
    $scope.activeLedgersIcon = false;
    $scope.activeFinanceGroup = false;
    $scope.activeFinanceGroupIcon = false;
    $scope.activeBillComponentMaster = false;
    $scope.activeBillComponentMasterIcon = false;

    $scope.activeGroupAccount = false;
    $scope.activeGroupAccountIcon = false;
    $scope.activeGstType = false;
    $scope.activeGstTypeIcon = false;
    $scope.activeParty = false;
    $scope.activePartyIcon = false;
    $scope.activeMiscellaneous = false;
    $scope.activeMiscellaneousIcon = false;
    $scope.activeAccount = false;
    $scope.activeAccountIcon = false;

    $scope.activeCashTransaction = false;
    $scope.activeCashTransactionIcon = false;
    $scope.activeBankTransaction = false;
    $scope.activeBankTransactionIcon = false;
    $scope.activeJournalEntry = false;
    $scope.activeJournalEntryIcon = false;
    $scope.activePurchaseSalesEntries = false;
    $scope.activePurchaseSalesEntriesIcon = false;

    $scope.activePurchaseEntry = false;
    $scope.activePurchaseEntryIcon = false;
    $scope.activeReceipts = false;
    $scope.activeReceiptsIcon = false;
    $scope.activePayment = false;
    $scope.activePaymentIcon = false;
    $scope.activeContra = false;
    $scope.activeContraIcon = false;
    $scope.activeJournals = false;
    $scope.activeJournalsIcon = false;
    $scope.activeLedgerReport = false;
    $scope.activeLedgerReportIcon = false;
    $scope.activeIncomeAndExpenditure = false;
    $scope.activeIncomeAndExpenditureIcon = false;
    $scope.activeReceiptsPayments = false;
    $scope.activeReceiptsPaymentsIcon = false;
    $scope.activeProfitLoss = false;
    $scope.activeProfitLossIcon = false;
    $scope.activeTrialBalance = false;
    $scope.activeTrialBalanceIcon = false;
    $scope.activeBalanceSheet = false;
    $scope.activeSchedulesToBalanceSheet = false;
    $scope.activeSchedulesToBalanceSheetIcon = false;
    $scope.activeBalanceSheetIcon = false;
    $scope.activePurposeOfLoan = false;
    $scope.activePurposeOfLoanIcon = false;
    $scope.activeEconomicActivity = false;
    $scope.activeEconomicActivityIcon = false;
    $scope.activePurposeOfLoan = false;
    $scope.activePurposeOfLoanIcon = false;
    $scope.activeSendnotification = false;
    $scope.activeSendnotificationIcon = false;
    
    if(getValue == 'Dashboard') {
      $(".userControlSubMenu").removeClass("left-menu-list-opened");
      $(".getUserControlSubMenu").css("display","none");
      $(".masterSubMenu").removeClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","none");
      $(".operationsSubMenu").removeClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","none");
      $(".reportsSubMenu").removeClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","none");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      $(".financeSubMenu").removeClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","none");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
    } else if(getValue == 'Company') {
      $scope.activeCompany = true;
      $scope.activeCompanyIcon = true;
    } else if(getValue == 'MapMenu') {
      $(".userControlSubMenu").addClass("left-menu-list-opened");
      $(".getUserControlSubMenu").css("display","block");
      $scope.activeMapMenu = true;
      $scope.activeMapMenuIcon = true;
    } else if(getValue == 'custommenuname') {
      $(".userControlSubMenu").addClass("left-menu-list-opened");
      $(".getUserControlSubMenu").css("display","block");
      $scope.activeisCustommapname = true;
      $scope.activeisCustommapnameIcon = true;
    } else if(getValue == 'AssignUserRights') {
      $(".userControlSubMenu").addClass("left-menu-list-opened");
      $(".getUserControlSubMenu").css("display","block");
      $scope.activeAssignUserRights = true;
      $scope.activeAssignUserRightsIcon = true;
    }else if(getValue == 'Area') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeArea = true;
      $scope.activeAreaIcon = true;
    } else if(getValue == 'Branch') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeBranch = true;
      $scope.activeBranchIcon = true;
    } else if(getValue == 'Staff') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeStaff = true;
      $scope.activeStaffIcon = true;
    }  else if(getValue == 'Group') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeGroup = true;
      $scope.activeGroupIcon = true;
    }  else if(getValue == 'ApplicationAmount') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeApplicationAmount = true;
      $scope.activeApplicationAmountIcon = true;
    } else if(getValue == 'Member') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeMember = true;
      $scope.activeMemberIcon = true;
    } else if(getValue == 'Product') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeProduct = true;
      $scope.activeProductIcon = true;
    } else if(getValue == 'Funder') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeFunder = true;
      $scope.activeFunderIcon = true;
    } else if(getValue == 'Thrift') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeThrift = true;
      $scope.activeThriftIcon = true;
    } else if(getValue == 'EconomicActivity') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeEconomicActivity = true;
      $scope.activeEconomicActivityIcon = true;
    } else if(getValue == 'PurposeOfLoan') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
       $scope.activePurposeOfLoan = true;
       $scope.activePurposeOfLoanIcon = true;
    } else if(getValue == 'Sendnotification') {
      $(".masterSubMenu").addClass("left-menu-list-opened");
      $(".getMasterSubMenu").css("display","block");
      
      $scope.activeSendnotification = true;
      $scope.activeSendnotificationIcon = true;
    } else if(getValue == 'FundDetails') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeFundDetails = true;
      $scope.activeFundDetailsIcon = true;
    } else if(getValue == 'LoanDisbursement') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeLoanDisbursement = true;
      $scope.activeLoanDisbursementIcon = true;
    } else if(getValue == 'BonusLoanDisbursement') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeBonusLoanDisbursement = true;
      $scope.activeBonusLoanDisbursementIcon = true;
    } else if(getValue == 'GroupCollection') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeGroupCollection = true;
      $scope.activeGroupCollectionIcon = true;
    } else if(getValue == 'TodayReport') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeTodayReport = true;
      $scope.activeTodayReportIcon = true;
    } else if(getValue == 'daywiseCollection') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activedaywiseCollection = true;
      $scope.activedaywiseCollectionIcon = true;
    } else if(getValue == 'ThriftCollection') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeThriftCollection = true;
      $scope.activeThriftCollectionIcon = true;
    } else if(getValue == 'MapThriftmember') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeMapthriftmember = true;
      $scope.activeMapthriftmemberIcon = true;
    } else if(getValue == 'PreCloseLoan') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activePreCloseLoan = true;
      $scope.activePreCloseLoanIcon = true;
    } else if(getValue == 'MemberCollection') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeMemberCollection = true;
      $scope.activeMemberCollectionIcon = true;
    } else if(getValue == 'GroupMemberCollection') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeGroupMemberCollection = true;
      $scope.activeGroupMemberCollectionIcon = true;
    } else if(getValue == 'TodayCollections') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
      
      $scope.activeTodayCollections = true;
      $scope.activeTodayCollectionsIcon = true;  
    } else if(getValue == 'ChangeGroupOfficer') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
    
      $scope.activeChangeGroupOfficer = true;
      $scope.activeChangeGroupOfficerIcon = true;

    } else if(getValue == 'ChangeMeetingDate') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
    
      $scope.activeChangeMeetingDate = true;
      $scope.activeChangeMeetingDateIcon = true;

    } else if(getValue == 'Officerinsentive') {
      $(".operationsSubMenu").addClass("left-menu-list-opened");
      $(".getOperationsSubMenu").css("display","block");
    
      $scope.activeOfficerinsentive = true;
      $scope.activeOfficerinsentiveIcon = true;

    } else if(getValue == 'ProductReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeProductReport = true;
      $scope.activeProductReportIcon = true;
    } else if(getValue == 'ActivityReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeActivityReport = true;
      $scope.activeActivityReportIcon = true;
    } else if(getValue == 'AreaReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeAreaReport = true;
      $scope.activeAreaReportIcon = true;
    } else if(getValue == 'BranchReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeBranchReport = true;
      $scope.activeBranchReportIcon = true;
    } else if(getValue == 'MemberDetailsReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activememberDetailsreport = true;
      $scope.activememberDetailsreportIcon = true;
    } else if(getValue == 'GroupOfficerReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").addClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeGroupOfficerReport = true;
      $scope.activeGroupOfficerReportIcon = true;
    } else if(getValue == 'todayCollectionsReports') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeTodayCollectionsReports = true;
      $scope.activeTodayCollectionsReportsIcon = true;
    } else if(getValue == 'DayWiseCollectionsReports') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeDayWiseCollectionsReports = true;
      $scope.activeDayWiseCollectionsReportsIcon = true;
    }  else if(getValue == 'DayWiseGroupCollectionsReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeDayWiseGroupCollectionsReport = true;
      $scope.activeDayWiseGroupCollectionsReportIcon = true;
    } else if(getValue == 'ActiveMembers') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeMembers = true;
      $scope.activeMembersIcon = true;
    } else if(getValue == 'Insurance') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeInsurance = true;
      $scope.activeInsuranceIcon = true;
    } else if(getValue == 'InactiveMembers') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeInActiveMember = true;
      $scope.activeInActiveMemberIcon = true;
    } else if(getValue == 'ClosedLoanDetails') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeClosedLoanDetails = true;
      $scope.activeClosedLoanDetailsIcon = true;  
    } else if(getValue == 'BranchAssessments') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeBranchAssessments = true;
      $scope.activeBranchAssessmentsIcon = true;
    } else if(getValue == 'OverdueReports') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeOverdueReports = true;
      $scope.activeOverdueReportsIcon = true; 
    } else if(getValue == 'FunderReports') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeFunderReports = true;
      $scope.activeFunderReportsIcon = true;   
    } else if(getValue == 'RepaymentCollection') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeRepaymentCollection = true;
      $scope.activeRepaymentCollectionIcon = true;  
    } else if(getValue == 'ExpectedDemand') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeExpectedDemand = true;
      $scope.activeExpectedDemandIcon = true;    
    } else if(getValue == 'DemandCollection') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeDemandCollection = true;
      $scope.activeDemandCollectionIcon = true;  
    }  else if(getValue == 'LoanDisbursementReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $scope.activeLoanDisbursementReport = true;
      $scope.activeLoanDisbursementReportIcon = true;
    } else if(getValue == 'LoanCard') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
      
      $rootScope.activeLoanCard = true;
      $rootScope.activeLoanCardIcon = true;
    } else if(getValue == 'ThriftReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").removeClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","none");
  
      $scope.activeThriftReport = true;
      $scope.activeThriftReportIcon = true;
    } else if(getValue == 'PreCloserGroupReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").addClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      
      $scope.activePreCloserGroupReport = true;
      $scope.activePreCloserGroupReportIcon = true;  
    } else if(getValue == 'PreCloserMemberReport') {
      $(".reportsSubMenu").addClass("left-menu-list-opened");
      $(".getReportsSubMenu").css("display","block");
      $(".preCloserReportsMenu").addClass("left-menu-list-opened");
      $(".getPreCloserReportsSubMenu").css("display","block");
      $(".olbBasedReportsMenu").removeClass("left-menu-list-opened");
      $(".getOlbBasedReportsSubMenu").css("display","none");
      
      $scope.activePreCloserMemberReport = true;
      $scope.activePreCloserMemberReportIcon = true;  
    } else if(getValue == 'Ledgers') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      
      $scope.activeLedgers = true;
      $scope.activeLedgersIcon = true;
    } else if(getValue == 'FinanceGroup') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      
      $scope.activeFinanceGroup = true;
      $scope.activeFinanceGroupIcon = true;
    } else if(getValue == 'BillComponentMaster') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");

      $scope.activeBillComponentMaster = true;
      $scope.activeBillComponentMasterIcon = true;
    } else if(getValue == 'GroupAccount') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeGroupAccount = true;
      $scope.activeGroupAccountIcon = true;
    } else if(getValue == 'GstType') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeGstType = true;
      $scope.activeGstTypeIcon = true;
    } else if(getValue == 'Party') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");

      $scope.activeParty = true;
      $scope.activePartyIcon = true;
    } else if(getValue == 'Instrument') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");

      $scope.activeInstrument = true;
      $scope.activeInstrumentIcon = true;
    } else if(getValue == 'Miscellaneous') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeMiscellaneous = true;
      $scope.activeMiscellaneousIcon = true;
    } else if(getValue == 'Account') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").addClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","block");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeAccount = true;
      $scope.activeAccountIcon = true;
    } else if(getValue == 'CashTransaction') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").addClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","block");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeCashTransaction = true;
      $scope.activeCashTransactionIcon = true;
    } else if(getValue == 'BankTransaction') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").addClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","block");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeBankTransaction = true;
      $scope.activeBankTransactionIcon = true;
    }  else if(getValue == 'JournalEntry') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").addClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","block");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeJournalEntry = true;
      $scope.activeJournalEntryIcon = true;
    }  else if(getValue == 'PurchaseSalesEntries') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".getFinanceMastersMenu").addClass("left-menu-list-opened");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").addClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","block");
      
      $scope.activePurchaseSalesEntries = true;
      $scope.activePurchaseSalesEntriesIcon = true;
    } else if(getValue == 'PurchaseEntry') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activePurchaseEntry = true;
      $scope.activePurchaseEntryIcon = true;
    } else if(getValue == 'Receipts') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeReceipts = true;
      $scope.activeReceiptsIcon = true;
    } else if(getValue == 'Payment') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activePayment = true;
      $scope.activePaymentIcon = true;
    }else if(getValue == 'Contra') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeContra = true;
      $scope.activeContraIcon = true;
    } else if(getValue == 'Journals') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").removeClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","none");
      $scope.activeJournals = true;
      $scope.activeJournalsIcon = true;
    }  else if(getValue == 'LedgerReport') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");
      $scope.activeLedgerReport = true;
      $scope.activeLedgerReportIcon = true;
    } else if(getValue == 'IncomeAndExpenditure') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");
      $scope.activeIncomeAndExpenditure = true;
      $scope.activeIncomeAndExpenditureIcon = true;
    } else if(getValue == 'ReceiptsPayments') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");
      $scope.activeReceiptsPayments = true;
      $scope.activeReceiptsPaymentsIcon = true;
    } else if(getValue == 'ProfitLoss') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");

      $scope.activeProfitLoss = true;
      $scope.activeProfitLossIcon = true;
    } else if(getValue == 'TrialBalance') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");  
      $scope.activeTrialBalance = true;
      $scope.activeTrialBalanceIcon = true;
    } else if(getValue == 'BalanceSheet') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");
      $scope.activeBalanceSheet = true;
      $scope.activeBalanceSheetIcon = true;
    } else if(getValue == 'SchedulesToBalanceSheet') {
      $(".financeSubMenu").addClass("left-menu-list-opened");
      $(".getfinanceSubMenu").css("display","block");
      $(".financeMastersMenu").removeClass("left-menu-list-opened");
      $(".getFinanceMastersSubMenu").css("display","none");
      $(".financeTransactionsMenu").removeClass("left-menu-list-opened");
      $(".getFinanceTransactionsSubMenu").css("display","none");
      $(".financeReportMenu").addClass("left-menu-list-opened");
      $(".getFinanceReportsSubMenu").css("display","block");
      $scope.activeSchedulesToBalanceSheet = true;
      $scope.activeSchedulesToBalanceSheetIcon = true;
    } 
  }

});