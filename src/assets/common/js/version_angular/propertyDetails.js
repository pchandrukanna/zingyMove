app.controller('propertyDetailsController', function($routeParams, $timeout, $http, $rootScope, $window, $scope, $sce, $compile) {
    rating(".visitor-rating");   
    
    var propID = $routeParams.propID;
    $scope.propertyID = $routeParams.propID;
    var requestType = $routeParams.type;
    $scope.showContactBtn = false;
    $scope.showAppointmentDetails = false;
    $scope.showReviewsBlock = false;
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        $scope.userEmail = getZingyMoveUserData[0].userEmail;
        $scope.userPhone = getZingyMoveUserData[0].userPhone;
        
        if($scope.userType == 'Renter') {
            $scope.showContactBtn = true;
        }
    }

    var req = {
        method: 'POST',
        url: domainAddress+'GetPropertyDetails/'+propID,
    }
    $http(req).then(function(resp) {
        var response = resp.data;
        if(response.status === "success"){
            $scope.propDetails = response.records[0];
            var records = response.records[0];
            
            if($scope.propDetails.appointments != undefined) {
                var appointments = $scope.propDetails.appointments;
                for(var i = 0; i < appointments.length; i++) {
                    if($scope.userID == appointments[i].appointmentFrom) {
                        $scope.showContactBtn = false;
                        $scope.appointment = appointments[i];
                        if(appointments[i].appointmentStatus == 'Accept') {
                            $scope.showAppointmentDetails = true;
                        } else if(appointments[i].appointmentStatus == 'Reject') {
                            $scope.showAppointmentDetails = true;
                        } else if(appointments[i].appointmentStatus == 'Visited') {
                            $scope.showReviewsBlock = true;
                        } else if(appointments[i].appointmentStatus == 'Rated') {
                            $scope.showReviewsBlock = false;
                        }
                    }
                }
            }
            
            $scope.propImages = response.records[0].propertyImages;
            if(response.records[0].propertyImages != null && response.records[0].propertyImages != undefined) {
                for(var i = 0; i < $scope.propImages.length; i++) {
                    $scope.propImages[i] = domainAddress+$scope.propImages[i];
                }
            } else {
                $scope.noPropImages = true;
            }
            
            $scope.isMonday = records.isModay;
            $scope.mondayAMFrom = records.mondayAMFrom;
            $scope.mondayAMTill = records.mondayAMTill;
            $scope.mondayPMFrom = records.mondayPMFrom;
            $scope.mondayPMTill = records.mondayPMTill;
            
            $scope.isTuesday = records.isTuesday;
            $scope.tuesdayAMFrom = records.tuesdayAMFrom;
            $scope.tuesdayAMTill = records.tuesdayAMTill;
            $scope.tuesdayPMFrom = records.tuesdayPMFrom;
            $scope.tuesdayPMTill = records.tuesdayPMTill;

            $scope.isWednesday = records.isWednesday;
            $scope.wednesdayAMFrom = records.wednesdayAMFrom;
            $scope.wednesdayAMTill = records.wednesdayAMTill;
            $scope.wednesdayPMFrom = records.wednesdayPMFrom;
            $scope.wednesdayPMTill = records.wednesdayPMTill;

            $scope.isThursday = records.isThursday;
            $scope.thursdayAMTill = records.thursdayAMTill;
            $scope.thursdayAMFrom = records.thursdayAMFrom;
            $scope.thursdayPMFrom = records.thursdayPMFrom;
            $scope.thursdayPMTill = records.thursdayPMTill;

            $scope.isFriday = records.isFriday;
            $scope.fridayAMFrom = records.fridayAMFrom;
            $scope.fridayAMTill = records.fridayAMTill;
            $scope.fridayPMFrom = records.fridayPMFrom;
            $scope.fridayPMTill = records.fridayPMTill;

            $scope.isSaturday = records.isSaturday;
            $scope.saturdayAMFrom = records.saturdayAMFrom;
            $scope.saturdayAMTill = records.saturdayAMTill;
            $scope.saturdayPMFrom = records.saturdayPMFrom;
            $scope.saturdayPMTill = records.saturdayPMTill;

            $scope.isSunday = records.isSunday;
            $scope.sundayAMFrom = records.sundayAMFrom;
            $scope.sundayAMTill = records.sundayAMTill;
            $scope.sundayPMFrom = records.sundayPMFrom;
            $scope.sundayPMTill = records.sundayPMTill;

            //  For location in Map  //
            var _latitude = response.records[0].propertyLat;
            var _longitude = response.records[0].propertyLong;
            var element = "map-detail";
            simpleMap(_latitude,_longitude, element);
        } else {
            console.log("Error in Fetching Properties");
        }
    });

    if($scope.userType != "Owner" && $scope.userType != "Admin" && $scope.userType != "Super Admin") {
        var req = {
            method: 'POST',
            url: domainAddress+"UpdatePropertyView/"+propID
        }
        $http(req).then(function(resp) {
            if(resp.data.status == 'success') {
                console.log("Success");
            }
        });
    }

    if(requestType == 'contactOwner') {
        $("#contactOwnerModal").modal("toggle");
        // var startingDay = new Date();
        // var thisDay = new Date();
        // for(var i=0; i<7; i++) {
        //     thisDay.setDate(startingDay.getDate() + i);
        //     console.log(thisDay);
        // }
    } else if(requestType == 'saveProperty'){
        var postData = '{"propertyID":"'+propID+'","userID":"'+$scope.userID+'"}';
        var req = {
            method: 'POST',
            url: domainAddress+'SaveProperty',
            data: postData
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "Success"){
               setTimeout(() => {
                    swal("Success!", "Property Saved", "success");
                },300);
            } else {
                console.log("Error in creating appointment");
            }
        });
    }
    $scope.contactOwnerfunction = function() {
        $("#contactOwnerModal").modal("toggle");
    }

    $scope.submitContactOwner = function() {
        var dateFormat = $("#contactDate").val().split("-").reverse().join("-");

        if($("#contactDate").val() == '' || $("#contactDate").val() == undefined || $("#contactDate").val() == null) {
            $scope.errorContactDate = true;
        } else if($scope.appointmentTime == '' || $scope.appointmentTime == undefined || $scope.appointmentTime == null) {
            $scope.errorContactTime = true;
        } else {
            var postData = '{"appointmentOn":"'+dateFormat+'","appointmentTime":"'+$scope.appointmentTime+'","appointmentFrom":"'+$scope.userID+'","appointmentTo":"'+$scope.propDetails.userID+'","propetyID":"'+$scope.propDetails.propertyID+'","appointmentStatus":"","appointmentNotes":"","isConvertToLead":""}';
            
            console.log(postData);

            var req = {
                method: 'POST',
                url: domainAddress+'CreateAppointment',
                data: postData
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "Success"){
                    $("#contactOwnerModal").modal("toggle");
                    $scope.showContactBtn = false;
                    swal("Success!", "Appointment has been created!", "success");
                } else {
                    console.log("Error in creating appointment");
                }
            });
        }
    }
    $scope.showAvailableHours = false;
    
    var weekday = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
    $("#contactDate").on('dp.change', function(e) {
        var day = weekday[e.date._d.getDay()];
        var propDetails = $scope.propDetails;

        $scope.errorContactDate = false;

        if(day == 'Sunday') {
            if($scope.isSunday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.sundayAMFrom != undefined && propDetails.sundayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayAMFrom+" - "+propDetails.sundayAMTill);
                } else if(propDetails.sundayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayAMFrom);
                } else if(propDetails.sundayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayAMTill);
                }

                if(propDetails.sundayPMFrom != undefined && propDetails.sundayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayPMFrom+" - "+propDetails.sundayPMTill);
                } else if(propDetails.sundayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayPMFrom);
                } else if(propDetails.sundayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.sundayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Sundays!", "info");
            }
        } else if(day == 'Monday') {
            if($scope.isMonday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.mondayAMFrom != undefined && propDetails.mondayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayAMFrom+" - "+propDetails.mondayAMTill);
                } else if(propDetails.mondayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayAMFrom);
                } else if(propDetails.mondayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayAMTill);
                }

                if(propDetails.mondayPMFrom != undefined && propDetails.mondayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayPMFrom+" - "+propDetails.mondayPMTill);
                } else if(propDetails.mondayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayPMFrom);
                } else if(propDetails.mondayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.mondayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Mondays!", "info");
            }
        } else if(day == 'Tuesday') {
            if($scope.isTuesday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.tuesdayAMFrom != undefined && propDetails.tuesdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayAMFrom+" - "+propDetails.tuesdayAMTill);
                } else if(propDetails.tuesdayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayAMFrom);
                } else if(propDetails.tuesdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayAMTill);
                }

                if(propDetails.tuesdayPMFrom != undefined && propDetails.tuesdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayPMFrom+" - "+propDetails.tuesdayPMTill);
                } else if(propDetails.tuesdayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayPMFrom);
                } else if(propDetails.tuesdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.tuesdayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Tuesdays!", "info");
            }
        } else if(day == 'Wednesday') {
            if($scope.isWednesday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.wednesdayAMFrom != undefined && propDetails.wednesdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayAMFrom+" - "+propDetails.wednesdayAMTill);
                } else if(propDetails.wednesdayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayAMFrom);
                } else if(propDetails.wednesdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayAMTill);
                }

                if(propDetails.wednesdayPMFrom != undefined && propDetails.wednesdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayPMFrom+" - "+propDetails.wednesdayPMTill);
                } else if(propDetails.wednesdayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayPMFrom);
                } else if(propDetails.wednesdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.wednesdayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Wednesdays!", "info");
            }
        } else if(day == 'Thursday') {
            if($scope.isThursday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.thursdayAMFrom != undefined && propDetails.thursdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayAMFrom+" - "+propDetails.thursdayAMTill);
                } else if(propDetails.thursdayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayAMFrom);
                } else if(propDetails.thursdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayAMTill);
                }

                if(propDetails.thursdayPMFrom != undefined && propDetails.thursdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayPMFrom+" - "+propDetails.thursdayPMTill);
                } else if(propDetails.thursdayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayPMFrom);
                } else if(propDetails.thursdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.thursdayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Thursdays!", "info");
            }
        } else if(day == 'Friday') {
            if($scope.isFriday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.fridayAMFrom != undefined && propDetails.fridayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayAMFrom+" - "+propDetails.fridayAMTill);
                } else if(propDetails.fridayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayAMFrom);
                } else if(propDetails.fridayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayAMTill);
                }

                if(propDetails.fridayPMFrom != undefined && propDetails.fridayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayPMFrom+" - "+propDetails.fridayPMTill);
                } else if(propDetails.fridayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayPMFrom);
                } else if(propDetails.fridayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.fridayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Fridays!", "info");
            }
        } else {
            if($scope.isSaturday == '1') {
                $scope.availableHoursArr = [];
                if(propDetails.saturdayAMFrom != undefined && propDetails.saturdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayAMFrom+" - "+propDetails.saturdayAMTill);
                } else if(propDetails.saturdayAMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayAMFrom);
                } else if(propDetails.saturdayAMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayAMTill);
                }

                if(propDetails.saturdayPMFrom != undefined && propDetails.saturdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayPMFrom+" - "+propDetails.saturdayPMTill);
                } else if(propDetails.saturdayPMFrom != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayPMFrom);
                } else if(propDetails.saturdayPMTill != undefined) {
                    $scope.availableHoursArr.push(propDetails.saturdayPMTill);
                }
            } else {
                $scope.availableHoursArr = [];
                swal("Oops!", "No Schedules available on Saturdays!", "info");
            }
        }
        $scope.options = ['var1', 'var2', 'var3'];
        if($scope.availableHoursArr.length > 0) {
            // console.log($scope.availableHoursArr);
            var timeSelectContent = '<select ng-model="appointmentTime" ng-change="onTimeChange(appointmentTime)"><option value="">Select Time</option>';
            for(var i = 0; i < $scope.availableHoursArr.length; i++) {
                timeSelectContent += '<option value="'+$scope.availableHoursArr[i]+'">'+$scope.availableHoursArr[i]+'</option>';
            }
            timeSelectContent += '</select>';

            timeSelectContent +='<span style="color: red" ng-show="errorContactTime">* Please Select Appointment Time</span>';
            // $scope.showAvailableHours = true;
            var $el = $(timeSelectContent);
            $compile($el)($scope);              
            $("#timeSelectBox").html($el);
            $("#timeSelectBox").show();
        } else {
            $("#timeSelectBox").hide();
            $scope.appointmentTime = '';
        }
    });

    $scope.onTimeChange = function(contactTime) {
        if(contactTime != '' && onTimeChange != undefined && onTimeChange != null) {
            $scope.errorContactTime = false;
        } else {
            $scope.errorContactTime = true;
        }
    }

    $scope.submitReview = function() {
        // console.log($scope.reviewTitle+" - "+$scope.reviewMessege);
        var locationRating = parseInt($("#location").find(".active").length);
        var facilitiesRating = parseInt($("#facilities").find(".active").length);
        var internalConditionRating = parseInt($("#internalCondition").find(".active").length);
        var externalConditionRating = parseInt($("#externalCondition").find(".active").length);

        var overallRating = (locationRating+facilitiesRating+internalConditionRating+externalConditionRating)/4;
        // console.log(locationRating+" - "+facilitiesRating+" - "+internalConditionRating+" - "+externalConditionRating);
        // console.log(overallRating);

        if($scope.reviewTitle == '' || $scope.reviewTitle == undefined) {
            $scope.errReviewTitle = true;
        } else if($scope.reviewMessege == '' || $scope.reviewMessege == undefined) {
            $scope.errReviewMessege = true;
        } else {
            var postData = '{"rating":"'+overallRating+'","notes":"'+escape($scope.reviewMessege)+'","notesTitle":"'+$scope.reviewTitle+'","userID":"'+$scope.userID+'","propertyID":"'+$scope.propertyID+'","appointmentID":"'+$scope.appointment.appointmentID+'"}';
            console.log(postData);
            var req = {
                method: 'POST',
                url: domainAddress+'CreateInternalNotes',
                data: postData
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "Success"){
                    swal("Success!", "Notes Saved!", "success");
                    $scope.showReviewsBlock = false;
                } else {
                    console.log("Error in Submitting Notes");
                }
            });
        }
    }

    $scope.onReviewTitleChange = function(reviewTitle) {
        if(reviewTitle == '' || reviewTitle == null || reviewTitle == undefined) {
            $scope.errReviewTitle = true;
        } else {
            $scope.errReviewTitle = false;
        }
    }
    
    $scope.onReviewMessageChange = function(reviewMessage) {
        if(reviewMessage == '' || reviewMessage == null || reviewMessage == undefined) {
            $scope.errReviewMessage = true;
        } else {
            $scope.errReviewMessage = false;
        }
    }

    $scope.escape = function (str) {
        return str
            .replace(/[\\]/g, '\\\\')
            .replace(/[\"]/g, '\\\"')
            .replace(/[\/]/g, '\\/')
            .replace(/[\b]/g, '\\b')
            .replace(/[\f]/g, '\\f')
            .replace(/[\n]/g, '\\n')
            .replace(/[\r]/g, '\\r')
            .replace(/[\t]/g, '\\t');
    }

});

