app.controller('allPropertiesController', function($routeParams, $timeout, $http, $rootScope, $window, $scope, $sce) {
   
    $scope.noProperty = false;
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        $scope.userEmail = getZingyMoveUserData[0].userEmail;
        $scope.userPhone = getZingyMoveUserData[0].userPhone;
        
        if($scope.userType == 'Renter') {
            $scope.showContactBtn = true;
        }
    }

    GetAllproperties();

    function GetAllproperties() {
        var req = {
            method: 'POST',
            url: domainAddress+'GetAllPropertiesAdmin',
        }
        $http(req).then(function(resp) {
            
            var response = resp.data;
            if(response.status === "success"){
                var records = response.records;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                        for(var j = 0; j < records[i].propertyImages.length; j++) {
                            records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                        }
                    }
                }
                $scope.resultAllProperties = records;
                
                var x = 0;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].appointments != undefined) {
                        var appointments = records[i].appointments;
                        for(var j = 0; j < appointments.length; j++) {
                            if($scope.userID == appointments[j].appointmentFrom) {
                                
                                $scope.appointment = appointments[j];
                                console.log(appointments[j].appointmentStatus);
                                // if(appointments[j].appointmentStatus != 'Visited' && appointments[j].appointmentStatus != 'Rated') {
                                    $scope.resultAllProperties[i].appointmentUser = appointments[j];
                                // }
                            }
                        }
                    }
                }
                $scope.noProperty = false;
            } else {
                console.log("Failed");
                $scope.noProperty = true;
            }
        });
    }

    $scope.goToDetailsPage = function(propertyID) {
        window.location.href="#!/PropertyDetails?propID="+propertyID;
        window.location.reload();
    }
    $scope.textSearchFunction = function(searchText) {
        if(searchText != undefined && searchText != '') {
            var req = {
                method: 'POST',
                url: domainAddress+'GlobalSearchProperty/'+searchText,
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                            for(var j = 0; j < records[i].propertyImages.length; j++) {
                                records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                            }
                        }
                    }
                    $scope.resultAllProperties = records;
                    var x = 0;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].appointments != undefined) {
                            var appointments = records[i].appointments;
                            for(var j = 0; j < appointments.length; j++) {
                                if($scope.userID == appointments[j].appointmentFrom) {
                                    $scope.appointment = appointments[j];
                                    $scope.resultAllProperties[i].appointmentUser = appointments[j];
                                }
                            }
                        }
                    }
                    $scope.noProperty = false;
                } else {
                    console.log("Failed");
                    $scope.noProperty = true;
                }
            });
        } else {
            var req = {
                method: 'GET',
                url: domainAddress+'GetAllPropertyList',
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                            for(var j = 0; j < records[i].propertyImages.length; j++) {
                                records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                            }
                        }
                    }
                    $scope.resultAllProperties = records;
                    var x = 0;
                    for(var i = 0; i < records.length; i++) {
                        if(records[i].appointments != undefined) {
                            var appointments = records[i].appointments;
                            for(var j = 0; j < appointments.length; j++) {
                                if($scope.userID == appointments[j].appointmentFrom) {
                                    $scope.appointment = appointments[j];
                                        $scope.resultAllProperties[i].appointmentUser = appointments[j];
                                }
                            }
                        }
                    }
                    $scope.noProperty = false;
                } else {
                    console.log("Failed");
                    $scope.noProperty = true;
                }
            });
        }
    }

    $scope.approveProperty = function($propertyID) {
        var req = {
            method: 'POST',
            url: domainAddress+'ApproveProperty/'+$propertyID,
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            if(response.status === "success"){
                GetAllproperties();
                swal("Success!","Property Approved","success");
            } else {
                swal("Oops!","Something Went Wrong!","error");
            }
        });
    }
});