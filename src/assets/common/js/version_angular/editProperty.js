app.controller('editPropertyController', function($timeout, $http, $rootScope, $window, $scope, $sce, $routeParams) {
    $scope.propertySuccess = false;
    var propID = $routeParams.propID;

    $(".select2").select2();
    $(".select2").attr("style","width: 100%");

    $scope.dateToday = new Date();
    loadStates('+44');

    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        $scope.userEmail = getZingyMoveUserData[0].userEmail;
        $scope.userPhone = getZingyMoveUserData[0].userPhone;
    }

    $scope.isMonday = false;
    $scope.isTuesday = false;
    $scope.isWednesday = false
    $scope.isThursday = false;
    $scope.isFriday = false;
    $scope.isSaturday = false;
    $scope.isSunday = false;
    $scope.isAllDays = false;

    $scope.errorPropertyTitle = false;
    $scope.errorPropertyType = false;
    $scope.errorPropertyDescription = false;
    $scope.errorPropertyAddress = false;
    $scope.errorCity = false;
    $scope.errorPropPincode = false;
    $scope.errorCountry = false;
    $scope.errorState = false;
    $scope.errorPropertyPrice = false;
    $scope.errorPropertyFacing = false;
    $scope.errorPropertyLatLong = false;
    $scope.errorPropImages = false;
    $scope.errorPropertyFilter = false;

    $scope.errorFloorCovering = false;
    $scope.errorInternalCondition = false;
    $scope.errorExternalCondition = false;

    $scope.errorPropertyAge = false;
    $scope.errorNoOfParking = false;
    $scope.errorNoOfBedRooms = false;

    $scope.errorNoOfBathRooms = false;
    $scope.errorNoOfStudyRooms = false;
    $scope.errorNoOfReceptionRooms = false;

    $scope.errorCellarBasement = false;
    $scope.errorCableSatellite = false;
    $scope.errorBlindsCurtains = false;


    $('#modal-description').on('hidden.bs.modal', function () {
        var desc = $("#propertyDescription").val();
        if(desc == '') {
            $("#propertyDescription").focus();
            $scope.descModalClose = true;
        }
    });

    $scope.onTextDescFocus = function() {
        var desc = $("#propertyDescription").val();
        if(desc == '') {
            setTimeout(() => {
                if($scope.descModalClose == false) {
                    $("#modal-description").modal("toggle");
                }
            },500);
        }
    }

    $scope.selectDesc = function(block) {
        if(block == 1) {
            $scope.propertyDescription = $("#desc1").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        } else if(block == 2) {
            $scope.propertyDescription = $("#desc2").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        } else {
            $scope.propertyDescription = $("#desc3").html().replace(/\s\s+/g, ' ');
            $("#modal-description").modal("toggle");
        }
    }
    $("#descriptionTrigger").on("click", function() {
        $("#modal-description").modal("toggle");
    });

    

    $scope.onPropertyTitleChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyTitle = true;
        } else {
            $scope.errorPropertyTitle = false;
        }
    }

    $scope.onPropertyTypeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyType = true;
        } else {
            $scope.errorPropertyType = false;
        }
    }

    $scope.onPropertyDescriptionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyDescription = true;
        } else {
            $scope.errorPropertyDescription = false;
        }
    }

    $scope.onPropertyAddressChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyAddress = true;
        } else {
            $scope.errorPropertyAddress = false;
        }
    }

    // $scope.onPropLatLongChange = function() {
    //     // var propLat = $("#latitude").val();
    //     // var propLong = $("#longitude").val();
    //     // if(propLat == '' || propLong == '') {
    //     //     $scope.errorPropertyLatLong = true;
    //     // } else {
    //     //     $scope.errorPropertyLatLong = false;            
    //     // }
    // }

    $scope.onCityChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCity = true;
            $("#errorCity").html("*Select Property City");
        } else {
            $scope.errorCity = false;
            $("#errorCity").html("");
        }
    }

    $scope.onPinCodeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropPincode = true;
        } else {
            $scope.errorPropPincode = false;
        }
    }

    // $scope.onCountryChange = function(value) {
    //     if(value == '' || value == undefined) {
    //         $scope.errorCountry = true;
    //     } else {
    //         $scope.errorCountry = false;
    //     }
        
    //     if(value == 'India') {
    //         loadStates('+91');
    //     } else {
    //         loadStates('+44');
    //     }
    // }

    $scope.onStateChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorState = true;
            $("#errorState").html("*Select County");
        } else {
            $scope.errorState = false;
            $("#errorState").html("");
        }
        // loadCities(JSON.parse(value).StateName);
        loadCities(value);
    }

    $scope.onPriceChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyPrice = true;
        } else {
            $scope.errorPropertyPrice = false;
        }
    }

    $scope.onPropertyFacingChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyFacing = true;
        } else {
            $scope.errorPropertyFacing = false;
        }
    }

    // $scope.onPropImageChange = function(value) {
    //     if(value == '' || value == undefined) {
    //         $scope.errorPropImages = true;
    //     } else {
    //         $scope.errorPropImages = false;
    //     }
    // }

    $scope.onPropFilterChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyFilter = true;
        } else {
            $scope.errorPropertyFilter = false;
        }
    }

    $scope.onFloorCoveringChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorFloorCovering = true;
        } else {
            $scope.errorFloorCovering = false;
        }
    }

    $scope.onInternalConditionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorInternalCondition = true;
        } else {
            $scope.errorInternalCondition = false;
        }
    }

    $scope.onExternalConditionChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorExternalCondition = true;
        } else {
            $scope.errorExternalCondition = false;
        }
    }
    
    $scope.onPropertyAgeChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorPropertyAge = true;
        } else {
            $scope.errorPropertyAge = false;
        }
    }

    $scope.onNoOfParkingChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfParking = true;
        } else {
            $scope.errorNoOfParking = false;
        }
    }

    $scope.onNoOfBedRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfBedRooms = true;
        } else {
            $scope.errorNoOfBedRooms = false;
        }
    }

    $scope.onNoOfBathRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfBathRooms = true;
        } else {
            $scope.errorNoOfBathRooms = false;
        }
    }

    $scope.onNoOfStudyRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfStudyRooms = true;
        } else {
            $scope.errorNoOfStudyRooms = false;
        }
    }

    $scope.onNoOfReceptionRoomsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorNoOfReceptionRooms = true;
        } else {
            $scope.errorNoOfReceptionRooms = false;
        }
    }

    $scope.onCellarBasementChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCellarBasement = true;
        } else {
            $scope.errorCellarBasement = false;
        }
    }

    $scope.onCableSatelliteChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorCableSatellite = true;
        } else {
            $scope.errorCableSatellite = false;
        }
    }

    $scope.onBlindsCurtainsChange = function(value) {
        if(value == '' || value == undefined) {
            $scope.errorBlindsCurtains = true;
        } else {
            $scope.errorBlindsCurtains = false;
        }
    }

    var req = {
        method: 'POST',
        url: domainAddress+'GetPropertyDetails/'+propID,
    }
    $http(req).then(function(resp) {
        var response = resp.data;
        if(response.status === "success"){
            $scope.propDetails = response.records[0];
            console.log($scope.propDetails);

            // For Property Images //
            if(response.records[0].propertyImages != null && response.records[0].propertyImages != undefined) {
                $scope.propImages = response.records[0].propertyImages;
                $scope.propertyImageIDs = response.records[0].propertyImageIds;
                for(var i = 0; i < $scope.propImages.length; i++) {
                    $scope.propImages[i] = domainAddress+$scope.propImages[i];
                    $("#upload_Preview").append('<div class="imageContainer"><div class="fileRemove" id="propImage-'+$scope.propertyImageIDs[i]+'" onclick="angular.element(this).scope().removeImage('+$scope.propertyImageIDs[i]+')">x</div><img src='+$scope.propImages[i]+' class="img-responsive" /></div>');
                }
            }

            $("#propImageIds").val(response.records[0].propImgIds);

            
            var _latitude = $scope.propDetails.propertyLat;
            var _longitude = $scope.propDetails.propertyLong;
            var element = "map-submit";
            simpleMap(_latitude,_longitude, element, true);


            $scope.propertyTitle = $scope.propDetails.propertyTitle;
            $scope.propertyType = $scope.propDetails.propertyType;
            $scope.propertyDescription = $scope.propDetails.propertyDesc;
            $scope.propertyAddress = $scope.propDetails.propertyAddress;
            $scope.propertyLat = $scope.propDetails.propertyLat;
            $scope.propertyLong = $scope.propDetails.propertyLong;            
            $scope.propPincode = $scope.propDetails.propertyPostCode;

            // $scope.state = '"{StateName:"'+ $scope.propDetails.propertyCounty +'"}';
            // $scope.City = '"{CityName:"'+ $scope.propDetails.propertyCity +'"}';

            // $('#City').select2('data', {a_key: $scope.propDetails.propertyCity});

            $scope.propertyPrice = $scope.propDetails.propertyPrice;
            $scope.propertyFacing = $scope.propDetails.propertyFacing;
            $scope.propertyFilter = $scope.propDetails.propertyFilter;
            $scope.floorCovering = $scope.propDetails.floorCovering;
            $scope.internalCondition = $scope.propDetails.internalCondition;
            $scope.externalCondition = $scope.propDetails.externalCondition;
            $scope.propertyAge = $scope.propDetails.propertAge;
            $scope.noOfParking = $scope.propDetails.parkingNumber;
            $scope.noOfBedRooms = $scope.propDetails.bedroom;
            $scope.noOfBathRooms = $scope.propDetails.bathroom;
            $scope.noOfStudyRooms = $scope.propDetails.studyRoom;
            $scope.noOfReceptionRooms = $scope.propDetails.receptionRooms;
            $scope.cellarBasement = $scope.propDetails.cellarBasement;
            $scope.cableSatellite = $scope.propDetails.cableSatellite;
            $scope.blindsCurtains = $scope.propDetails.blindsCurtains;

            if($scope.propDetails.isModay == '1') {
                $scope.isMonday = true;
            }
            if($scope.propDetails.isTuesday == '1') {
                $scope.isTuesday = true;
            }
            if($scope.propDetails.isWednesday == '1') {
                $scope.isWednesday = true;
            }
            if($scope.propDetails.isThursday == '1') {
                $scope.isThursday = true;
            }
            if($scope.propDetails.isFriday == '1') {
                $scope.isFriday = true;
            }
            if($scope.propDetails.isSaturday == '1') {
                $scope.isSaturday = true;
            }
            if($scope.propDetails.isSunday == '1') {
                $scope.isSunday = true;
            }
            $scope.onDaysChecked(); 

            $('#mondayAMFrom').val($scope.propDetails.mondayAMFrom);
            $('#mondayAMTill').val($scope.propDetails.mondayAMTill);
            $('#mondayPMFrom').val($scope.propDetails.mondayPMFrom);
            $('#mondayPMTill').val($scope.propDetails.mondayPMTill);

            $('#tuesdayAMFrom').val($scope.propDetails.tuesdayAMFrom);
            $('#tuesdayAMTill').val($scope.propDetails.tuesdayAMTill);
            $('#tuesdayPMFrom').val($scope.propDetails.tuesdayPMFrom);
            $('#tuesdayPMTill').val($scope.propDetails.tuesdayPMTill);

            $('#wednesdayAMFrom').val($scope.propDetails.wednesdayAMFrom);
            $('#wednesdayAMTill').val($scope.propDetails.wednesdayAMTill);
            $('#wednesdayPMFrom').val($scope.propDetails.wednesdayPMFrom);
            $('#wednesdayPMTill').val($scope.propDetails.wednesdayPMTill);

            $('#thursdayAMFrom').val($scope.propDetails.thursdayAMFrom);
            $('#thursdayAMTill').val($scope.propDetails.thursdayAMTill);
            $('#thursdayPMFrom').val($scope.propDetails.thursdayPMFrom);
            $('#thursdayPMTill').val($scope.propDetails.thursdayPMTill);

            $('#fridayAMFrom').val($scope.propDetails.fridayAMFrom);
            $('#fridayAMTill').val($scope.propDetails.fridayAMTill);
            $('#fridayPMFrom').val($scope.propDetails.fridayPMFrom);
            $('#fridayPMTill').val($scope.propDetails.fridayPMTill);

            $('#saturdayAMFrom').val($scope.propDetails.saturdayAMFrom);
            $('#saturdayAMTill').val($scope.propDetails.saturdayAMTill);
            $('#saturdayPMFrom').val($scope.propDetails.saturdayPMFrom);
            $('#saturdayPMTill').val($scope.propDetails.saturdayPMTill);

            $('#sundayAMFrom').val($scope.propDetails.sundayAMFrom);
            $('#sundayAMTill').val($scope.propDetails.sundayAMTill);
            $('#sundayPMFrom').val($scope.propDetails.sundayPMFrom);
            $('#sundayPMTill').val($scope.propDetails.sundayPMTill);


            if($scope.propDetails.isFrontGarden === '1') {
                $scope.isFrontGarden = true;
            }
            if($scope.propDetails.isRearGarden === '1') {
                $scope.isRearGarden = true;
            }
            if($scope.propDetails.isExtractorFanBathroom === '1') {
                $scope.isExtractorFanBathroom = true;
            }
            if($scope.propDetails.isSeperateWC === '1') {
                $scope.isSeperateWC = true;
            }
            if($scope.propDetails.isEnsuite === '1') {
                $scope.isEnsuite = true;
            }
            if($scope.propDetails.utilityRoom === '1') {
                $scope.utilityRoom = true;
            }
            if($scope.propDetails.heatingGas === '1') {
                $scope.heatingGas = true;
            }
            if($scope.propDetails.heatingElectricty === '1') {
                $scope.heatingElectricty = true;
            }
            if($scope.propDetails.heatingOther === '1') {
                $scope.heatingOther = true;
            }
            if($scope.propDetails.smokeAlarm === '1') {
                $scope.smokeAlarm = true;
            }
            if($scope.propDetails.monoxideAlarm === '1') {
                $scope.monoxideAlarm = true;
            }
            if($scope.propDetails.chimney === '1') {
                $scope.chimney = true;
            }
            if($scope.propDetails.saniflo === '1') {
                $scope.saniflo = true;
            }
            if($scope.propDetails.doubleGlazing === '1') {
                $scope.doubleGlazing = true;
            }
            if($scope.propDetails.isKitchenWashingMachin === '1') {
                $scope.isKitchenWashingMachin = true;
            }
            if($scope.propDetails.isKitchenTumbleDryer === '1') {
                $scope.isKitchenTumbleDryer = true;
            }
            if($scope.propDetails.isKitchenOven === '1') {
                $scope.isKitchenOven = true;
            }
            if($scope.propDetails.isKitchenHob === '1') {
                $scope.isKitchenHob = true;
            }
            if($scope.propDetails.isExtractorFan === '1') {
                $scope.isExtractorFan = true;
            }



        } else {
            console.log("Error in Fetching Properties");
        }
    });

    $scope.updateProperty = function () {
        // $('#submitAddPropBtn').prop('disabled', true);
        var propertyLat = $("#latitude").val();
        var propertyLong = $("#longitude").val();
        var propImageIds = $("#propImageIds").val();

        if($scope.propertyTitle == '' || $scope.propertyTitle == undefined) {
            $scope.errorPropertyTitle = true;
            $("#propertyTitle").focus();
            return false;
        } else if($scope.propertyType == '' || $scope.propertyType == undefined) {
            $scope.errorPropertyType = true;
            $("#propertyType").focus();            
            return false;
        } else if($scope.propertyDescription == '' || $scope.propertyDescription == undefined) {
            $scope.errorPropertyDescription = true;
            $("#propertyDescription").focus();
            return false;
            
        } else if($scope.propertyAddress == '' || $scope.propertyAddress == undefined) {
            $scope.errorPropertyAddress = true;
            $("#address-autocomplete").focus();
            return false;
            
        } else if($scope.state == '' || $scope.state == undefined) {
            $scope.errorState = true;
            $("#errorState").html("*Select County");
            $("#state").focus();
            return false;
            
        } else if($scope.City == '' || $scope.City == undefined) {
            $scope.errorCity = true;
            $("#errorCity").html("*Select Property City");
            $("#City").focus();
            return false;
            
        } else if($("#pincode").val() == '' || $("#pincode").val() == undefined) {
            $scope.errorPropPincode = true;
            $("#pincode").focus();
            return false;
            
        } else if($scope.propertyPrice == '' || $scope.propertyPrice == undefined) {
            $scope.errorPropertyPrice = true;
            $("#propertyPrice").focus();
            return false;
            
        } else if($scope.propertyFacing == '' || $scope.propertyFacing == undefined) {
            $scope.errorPropertyFacing = true;
            $("#propertyFacing").focus();
            return false;
            
        // } else if($scope.propertyLat == '' || propertyLat == undefined || propertyLong == '' || propertyLong == undefined) {
        //     $scope.errorPropertyLatLong = true;
        //     $("#map-submit").focus();
        //     return false;
            
        // } else if(propImageIds == '' || propImageIds == undefined) {
        //     $scope.errorPropImages = true;
        //     $("#dropbox").focus();
        //     return false;
            
        } else if($scope.propertyFilter == '' || $scope.propertyFilter == undefined) {
            $scope.errorPropertyFilter = true;
            $("#propertyFilter").focus();
            return false;
            
        } else if($scope.floorCovering == '' || $scope.floorCovering == undefined) {
            $scope.errorFloorCovering = true;
            $("#floorCovering").focus();
            return false;
            
        } else if($scope.internalCondition == '' || $scope.internalCondition == undefined) {
            $scope.errorInternalCondition = true;
            $("#internalCondition").focus();
            return false;
            
        } else if($scope.externalCondition == '' || $scope.externalCondition == undefined) {
            $scope.errorExternalCondition = true;
            $("#externalCondition").focus();
            return false;
            
        } else if($scope.propertyAge == '' || $scope.propertyAge == undefined) {
            $scope.errorPropertyAge = true;
            $("#propertyAge").focus();
            return false;
            
        } else if($scope.noOfParking == '' || $scope.noOfParking == undefined) {
            $scope.errorNoOfParking = true;
            $("#noOfParking").focus();
            return false;
            
        } else if($scope.noOfBedRooms == '' || $scope.noOfBedRooms == undefined) {
            $scope.errorNoOfBedRooms = true;
            $("#noOfBedRooms").focus();
            return false;
            
        } else if($scope.noOfBathRooms == '' || $scope.noOfBathRooms == undefined) {
            $scope.errorNoOfBathRooms = true;
            $("#noOfBathRooms").focus();
            return false;
            
        } else if($scope.noOfStudyRooms == '' || $scope.noOfStudyRooms == undefined) {
            $scope.errorNoOfStudyRooms = true;
            $("#noOfStudyRooms").focus();
            return false;
            
        } else if($scope.noOfReceptionRooms == '' || $scope.noOfReceptionRooms == undefined) {
            $scope.errorNoOfReceptionRooms = true;
            $("#noOfReceptionRooms").focus();
            return false;
            
        } else if($scope.cellarBasement == '' || $scope.cellarBasement == undefined) {
            $scope.errorCellarBasement = true;
            $("#cellarBasement").focus();
            return false;
            
        } else if($scope.cableSatellite == '' || $scope.cableSatellite == undefined) {
            $scope.errorCableSatellite = true;
            $("#cableSatelite").focus();
            return false;
            
        } else if($scope.blindsCurtains == '' || $scope.blindsCurtains == undefined) {
            $scope.errorBlindsCurtains = true;
            $("#blindsCurtains").focus();
            return false;
            
        } else if($scope.mondayCheckedFalse && $scope.tuesdayCheckedFalse && $scope.wednesdayCheckedFalse && $scope.thursdayCheckedFalse && $scope.fridayCheckedFalse && $scope.saturdayCheckedFalse && $scope.sundayCheckedFalse) {
            $scope.errorOpeningHours = true;
            $('#accordion').focus();
        } else {

            if($scope.isFrontGarden == undefined) {
                $scope.isFrontGarden = 0;
            }
            if($scope.isRearGarden == undefined) {
                $scope.isRearGarden = 0;
            }
            if($scope.isExtractorFanBathroom == undefined) {
                $scope.isExtractorFanBathroom = 0;
            }
            if($scope.isSeperateWC == undefined) {
                $scope.isSeperateWC = 0;
            }
            if($scope.isEnsuite == undefined) {
                $scope.isEnsuite = 0;
            }
            if($scope.utilityRoom == undefined) {
                $scope.utilityRoom = 0;
            }
            if($scope.heatingGas == undefined) {
                $scope.heatingGas = 0;
            }
            if($scope.heatingElectricty == undefined) {
                $scope.heatingElectricty = 0;
            }
            if($scope.smokeAlarm == undefined) {
                $scope.smokeAlarm = 0;
            }
            if($scope.monoxideAlarm == undefined) {
                $scope.monoxideAlarm = 0;
            }
            if($scope.chimney == undefined) {
                $scope.chimney = 0;
            }
            if($scope.saniflo == undefined) {
                $scope.saniflo = 0;
            }
            if($scope.doubleGlazing == undefined) {
                $scope.doubleGlazing = 0;
            }
            if($scope.isKitchenWashingMachin == undefined) {
                $scope.isKitchenWashingMachin = 0;
            }
            if($scope.isKitchenTumbleDryer == undefined) {
                $scope.isKitchenTumbleDryer = 0;
            }
            if($scope.isKitchenOven == undefined) {
                $scope.isKitchenOven = 0;
            }
            if($scope.isKitchenHob == undefined) {
                $scope.isKitchenHob = 0;
            }
            if($scope.isExtractorFan == undefined) {
                $scope.isExtractorFan = 0;
            }

            var propertyAddress = $("#address-autocomplete").val();

            var openingHours = '"isMonday":'+$scope.isMonday+',"isTuesday":'+$scope.isTuesday+',"isWednesday":'+$scope.isWednesday+',"isThursday":'+$scope.isThursday+',"isFriday":'+$scope.isFriday+',"isSaturday":'+$scope.isSaturday+',"isSunday":'+$scope.isSunday+',"mondayAMFrom":"'+$("#mondayAMFrom").val()+'","mondayAMTill":"'+$("#mondayAMTill").val()+'","mondayPMFrom":"'+$("#mondayPMFrom").val()+'","mondayPMTill":"'+$("#mondayPMTill").val()+'","tuesdayAMFrom":"'+$("#tuesdayAMFrom").val()+'","tuesdayAMTill":"'+$("#tuesdayAMTill").val()+'","tuesdayPMFrom":"'+$("#tuesdayPMFrom").val()+'","tuesdayPMTill":"'+$("#tuesdayPMTill").val()+'","wednesdayAMFrom":"'+$("#wednesdayAMFrom").val()+'","wednesdayAMTill":"'+$("#wednesdayAMTill").val()+'","wednesdayPMFrom":"'+$("#wednesdayPMFrom").val()+'","wednesdayPMTill":"'+$("#wednesdayPMTill").val()+'","thursdayAMFrom":"'+$("#thursdayAMFrom").val()+'","thursdayAMTill":"'+$("#thursdayAMTill").val()+'","thursdayPMFrom":"'+$("#thursdayPMFrom").val()+'","thursdayPMTill":"'+$("#thursdayPMTill").val()+'","fridayAMFrom":"'+$("#fridayAMFrom").val()+'","fridayAMTill":"'+$("#fridayAMTill").val()+'","fridayPMFrom":"'+$("#fridayPMFrom").val()+'","fridayPMTill":"'+$("#fridayPMTill").val()+'","saturdayAMFrom":"'+$("#saturdayAMFrom").val()+'","saturdayAMTill":"'+$("#saturdayAMTill").val()+'","saturdayPMFrom":"'+$("#saturdayPMFrom").val()+'","saturdayPMTill":"'+$("#saturdayPMTill").val()+'","sundayAMFrom":"'+$("#sundayAMFrom").val()+'","sundayAMTill":"'+$("#sundayAMTill").val()+'","sundayPMFrom":"'+$("#sundayPMFrom").val()+'","sundayPMTill":"'+$("#sundayPMTill").val()+'"';

            var postData = '{"propertyID":"'+ propID +'","propertyTitle":"'+$scope.propertyTitle+'","propertyDesc":"'+$scope.propertyDescription+'","propertyAddress":"'+$scope.propertyAddress+'","propertyLat":"'+propertyLat+'","propertyLong":"'+propertyLong+'","propertyType":"'+$scope.propertyType+'","propertyFilter":"'+$scope.propertyFilter+'","propertyPrice":"'+$scope.propertyPrice+'","propertyCounty":"'+$scope.state+'","propertyCity":"'+$scope.City+'","propertyPostCode":"'+$scope.propPincode+'","bedroom":"'+$scope.noOfBedRooms+'","bathroom":"'+$scope.noOfBathRooms+'","studyRoom":"'+$scope.noOfStudyRooms+'","floorCovering":"'+$scope.floorCovering+'","flooringCoveringother":"","userID":"'+$scope.userID+'","isAvailable":"1","propertAge":"'+$scope.propertyAge+'","parkingNumber":"'+$scope.noOfParking+'","isFrontGarden":'+$scope.isFrontGarden+',"isRearGarden":'+$scope.isRearGarden+',"isExtractorFanBathroom":'+$scope.isExtractorFanBathroom+',"isSeperateWC":'+$scope.isSeperateWC+',"isEnsuite":'+$scope.isEnsuite+',"receptionRooms":"'+$scope.noOfReceptionRooms+'","studyRooms":"'+$scope.noOfStudyRooms+'","utilityRoom":'+$scope.utilityRoom+',"cellarBasement":"'+$scope.cellarBasement+'","heatingGas":'+$scope.heatingGas+',"heatingElectricty":'+$scope.heatingElectricty+',"heatingOther":"0","smokeAlarm":'+$scope.smokeAlarm+',"monoxideAlarm":'+$scope.monoxideAlarm+',"chimney":'+$scope.chimney+',"saniflo":'+$scope.saniflo+',"doubleGlazing":'+$scope.doubleGlazing+',"cableSatellite":"'+$scope.cableSatellite+'","internalCondition":"'+$scope.internalCondition+'","externalCondition":"'+$scope.externalCondition+'","blindsCurtains":"'+$scope.blindsCurtains+'","isKitchenWashingMachin":'+$scope.isKitchenWashingMachin+',"isKitchenTumbleDryer":'+$scope.isKitchenTumbleDryer+',"isKitchenOven":'+$scope.isKitchenOven+',"isKitchenHob":'+$scope.isKitchenHob+',"isExtractorFan":'+$scope.isExtractorFan+',"propertyFacing":"'+$scope.propertyFacing+'","propertyImageIds":"'+propImageIds+'",'+openingHours+'}';
            
            // console.log(postData);

            var req = {
                method: 'POST',
                url: domainAddress+'UpdateProperty',
                data: postData
            }
            $http(req).then(function(resp) {
                $scope.spinnerload='';
                var response = resp.data;
                if(response.status === "success"){
                    $('#addPropertyForm')[0].reset();
                    $scope.progressVisible = false;
                    $("#uploadProgress").hide();
                    $("#upload_Preview").html('');
                    $('#submitAddPropBtn').prop('disabled', false);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    // $scope.propertySuccess = true;
                    $scope.propertyTitle = '';
                    $scope.propertyType = '';
                    $scope.propertyDescription = '';
                    $scope.propertyAddress = '';
                    $scope.state = '';
                    $scope.City = '';
                    $scope.propPincode = '';
                    $scope.propertyPrice = ''
                    $scope.propertyFacing = ''
                    $scope.propertyFilter = '';
                    $scope.floorCovering = '';
                    $scope.internalCondition = '';
                    $scope.externalCondition = '';
                    $scope.propertyAge = '';
                    $scope.noOfParking = '';
                    $scope.noOfBedRooms = '';
                    $scope.noOfBathRooms = '';
                    $scope.noOfStudyRooms = '';
                    $scope.noOfReceptionRooms = '';
                    $scope.cellarBasement = '';
                    $scope.cableSatellite = '';
                    $scope.blindsCurtains = '';

                    $scope.errorPropertyTitle = false;
                    $scope.errorPropertyType = false;
                    $scope.errorPropertyDescription = false;
                    $scope.errorPropertyAddress = false;
                    $scope.errorCity = false;
                    $scope.errorPropPincode = false;
                    $scope.errorCountry = false;
                    $scope.errorState = false;
                    $scope.errorPropertyPrice = false;
                    $scope.errorPropertyFacing = false;
                    $scope.errorPropertyLatLong = false;
                    $scope.errorPropImages = false;
                    $scope.errorPropertyFilter = false;

                    $scope.errorFloorCovering = false;
                    $scope.errorInternalCondition = false;
                    $scope.errorExternalCondition = false;

                    $scope.errorPropertyAge = false;
                    $scope.errorNoOfParking = false;
                    $scope.errorNoOfBedRooms = false;

                    $scope.errorNoOfBathRooms = false;
                    $scope.errorNoOfStudyRooms = false;
                    $scope.errorNoOfReceptionRooms = false;

                    $scope.errorCellarBasement = false;
                    $scope.errorCableSatellite = false;
                    $scope.errorBlindsCurtains = false;
                    swal("Success!", "Property Updated Successfully!", "success");
                } else {
                    swal("Oops!", "Something went wrong!", "error");
                }
            });
        }
    }

    function loadStates(ContryCode) {
        var req = {
            method: 'GET',
            url: domainAddress+"CityState/getState.php?countryID="+ContryCode
        }
        $http(req).then(function(resp) {
            $scope.listStates = resp.data.records;
        });
    }

    function loadCities(stateCode) {
        var req = {
            method: 'GET',
            url: domainAddress+"CityState/getCity.php?stateID="+stateCode
        }
        $http(req).then(function(resp) {
            $scope.listCities = resp.data.records;
        });
    }


    $scope.removeImage = function(id) {
        $("#propImage-"+id).parent().remove();

        var req = {
            method: 'POST',
            url: domainAddress+"DeletePropertyImage/"+id
        }
        $http(req).then(function(resp) {
            if(resp.data.status == 'success') {
                console.log("Success");
            }
        });
    }

    $scope.onAllDaysChecked = function() {
        if($scope.isAllDays) {
            $scope.isMonday = true;
            $scope.isTuesday = true;
            $scope.isWednesday = true;
            $scope.isThursday = true;
            $scope.isFriday = true;
            $scope.isSaturday = true;
            $scope.isSunday = true;
        } else {
            $scope.isMonday = false;
            $scope.isTuesday = false;
            $scope.isWednesday = false;
            $scope.isThursday = false;
            $scope.isFriday = false;
            $scope.isSaturday = false;
            $scope.isSunday = false;
        }

        $scope.onDaysChecked(); 
    }

    $scope.onDaysChecked = function() {
        if($scope.isMonday) {
            $scope.mondayCheckedFalse = false;
        } else {
            $scope.mondayCheckedFalse = true;
        }
        if($scope.isTuesday) {
            $scope.tuesdayCheckedFalse = false;
        } else {
            $scope.tuesdayCheckedFalse = true;
        }
        if($scope.isWednesday) {
            $scope.wednesdayCheckedFalse = false;
        } else {
            $scope.wednesdayCheckedFalse = true;
        }
        if($scope.isThursday) {
            $scope.thursdayCheckedFalse = false;
        } else {
            $scope.thursdayCheckedFalse = true;
        }
        if($scope.isFriday) {
            $scope.fridayCheckedFalse = false;
        } else {
            $scope.fridayCheckedFalse = true;
        }
        if($scope.isSaturday) {
            $scope.saturdayCheckedFalse = false;
        } else {
            $scope.saturdayCheckedFalse = true;
        }
        if($scope.isSunday) {
            $scope.sundayCheckedFalse = false;
        } else {
            $scope.sundayCheckedFalse = true;
        }

        if($scope.isMonday && $scope.isTuesday && $scope.isWednesday && $scope.isThursday && $scope.isFriday && $scope.isSaturday && $scope.isSunday) {
            $scope.isAllDays = true;
        } else {
            $scope.isAllDays = false;
        }
    }

});

app.controller('editFileUploadCtrl', function($scope) {
    //============== DRAG & DROP =============
    // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
    $scope.userID = '';

    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
    }

    var dropbox = document.getElementById("dropbox")
    $scope.dropText = 'Drop Property Images here...'

    // init event handlers
    function dragEnterLeave(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
    }
    dropbox.addEventListener("dragenter", dragEnterLeave, false)
    dropbox.addEventListener("dragleave", dragEnterLeave, false)
    dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        var clazz = 'not-available'
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        $scope.$apply(function(){
            $scope.dropText = ok ? 'Drop Property Images here...' : 'Only files are allowed!'
            $scope.dropClass = ok ? 'over' : 'not-available'
        })
    }, false)
    dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
        evt.stopPropagation()
        evt.preventDefault()
        $scope.$apply(function(){
            $scope.dropText = 'Drop Property Images here...'
            $scope.dropClass = ''
        })
        var files = evt.dataTransfer.files
        if (files.length > 0) {
            $scope.$apply(function(){
                $scope.files = []
                for (var i = 0; i < files.length; i++) {
                    $scope.files.push(files[i])
                }
            })
        }

        setTimeout(() => {
            $scope.uploadFile();
        },300);
    }, false)
    //============== DRAG & DROP =============

    $scope.setFiles = function(element) {
        $scope.$apply(function($scope) {
            console.log('files:', element.files);
            // Turn the FileList object into an Array
                $scope.files = []
                for (var i = 0; i < element.files.length; i++) {
                $scope.files.push(element.files[i])
                }
            $scope.progressVisible = false;
            $("#uploadProgress").hide();
        });

        setTimeout(() => {
            $scope.uploadFile();
        },300);
    };

    $scope.uploadFile = function() {
        var fd = new FormData()
        for (var i in $scope.files) {
            fd.append("uploadedFile[]", $scope.files[i])
        }
        fd.append("userID",$scope.userID)
        // console.log($scope.files[0]);
        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        xhr.addEventListener("error", uploadFailed, false)
        xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", domainAddress+"UploadPropertyImage")
        $scope.progressVisible = true;
        $("#uploadProgress").show();
        xhr.send(fd)
    }

    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                $scope.progress = 'unable to compute'
            }
        })
    }

    function uploadComplete(evt) {
        var resp = JSON.parse(evt.target.responseText);
        console.log(resp.files+" -- "+resp.propImageIds);

        var propImages = $("#propImageIds").val();
        if(propImages != '') {
            propImages = propImages+','+resp.propImageIds;
            $("#propImageIds").val(propImages);
        } else {
            $("#propImageIds").val(resp.propImageIds);
        }        
        $("#propImageSrc").val(resp.files);

        var propImageIDArr = resp.propImageIds.split(',');
        for($i = 0; $i < resp.files.length; $i++) {
            $("#upload_Preview").append('<div class="imageContainer"><div class="fileRemove" id="propImage-'+propImageIDArr[$i]+'" onclick="angular.element(this).scope().removeImage('+propImageIDArr[$i]+')">x</div><img src='+domainAddress+resp.files[$i]+' class="img-responsive" /></div>');
        }
        $("#uploadProgress").hide();
    }

    function getRandomSize(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }

    function uploadCanceled(evt) {
        $scope.$apply(function(){
            $scope.progressVisible = false;
            $("#uploadProgress").hide();
        })
        alert("The upload has been canceled by the user or the browser dropped the connection.")
    }
});