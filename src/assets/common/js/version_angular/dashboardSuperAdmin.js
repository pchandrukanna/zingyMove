app.controller('superAdminDashboardController', function($timeout, $http, $rootScope, $window, $scope, $sce, $compile) {
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
        $scope.userEmail = getZingyMoveUserData[0].userEmail;
        $scope.userPhone = getZingyMoveUserData[0].userPhone;
        
        if($scope.userType == 'Renter') {
            $scope.showContactBtn = true;
        }
    }
    var req = {
        method: 'GET',
        url: domainAddress+'GetAllUsersList',
    }
    $http(req).then(function(resp) {
        var response = resp.data;
        if(response.status === "success"){
            if(resp.data.record_count > 0) {
                $scope.AllUsers = resp.data.records;
            } else {
                $scope.noUsers = true;
            }
        } else {
            swal("Oops!", "Something went wrong!", "error");
        }
    });

    $scope.goToUserDetails = function(user) {
        localStorage.setItem("CurrentUserDetails",JSON.stringify(user));
        window.location.href="#!/UserDetails";
    }
});