app.controller('listPropertyController', function($timeout, $http, $rootScope, $window, $scope, $sce) {
    $scope.userID = '';
    $scope.myProperties = [];
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
    }
    var req = {
        method: 'POST',
        url: domainAddress+'GetMyPropertiesForUser/'+$scope.userID,
    }
    $http(req).then(function(resp) {
        $scope.spinnerload='';
        var response = resp.data;
        if(response.status === "success"){
            var records = response.records;
            // console.log($scope.myProperties);
            for(var i = 0; i < records.length; i++) {
                for(var j = 0; j < records[i].propertyImages.length; j++) {
                    records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                }
            }
            $scope.myProperties = records;
        } else {
            console.log("Error in Fetching Properties");
        }
    });

    $scope.goToDetailsPage = function(propertyID) {
            window.location.href="#!/PropertyDetails?propID="+propertyID;
            window.location.reload();
    }
    $scope.goToEditPage = function(propertyID) {
            window.location.href="#!/EditProperty?propID="+propertyID;
            window.location.reload();
    }

    $scope.deleteProperty = function(propertyID) {
        var postData = '{"propertyID":"'+propertyID+'"}';
        
        console.log(postData);

        var req = {
            method: 'POST',
            url: domainAddress+'DeleteProperty',
            data: postData
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            console.log(response);
            if(response.status === "success"){
                console.log(response);
                location.reload();
            } else {
                console.log(response);
            }
        });
        
    }
});

