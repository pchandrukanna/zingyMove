app.controller('ownerDashboardController', function($timeout, $http, $rootScope, $window, $scope, $sce) {
   $scope.userID = '';
   $scope.noProperties = false;
    $scope.myProperties = [];
    var getZingyMoveUserData = JSON.parse(localStorage.getItem("zingyMoveUserInfo"));
    if(getZingyMoveUserData!=null){
        $scope.userID = getZingyMoveUserData[0].userID;
        $scope.userType = getZingyMoveUserData[0].userType;
    }
    var req = {
        method: 'POST',
        url: domainAddress+'GetMyPropertiesForUser/'+$scope.userID,
    }
    $http(req).then(function(resp) {
        $scope.spinnerload='';
        var response = resp.data;
        if(response.status === "success"){
            var records = response.records;
            if(response.record_count > 0) {
                $scope.noProperties = false;
                for(var i = 0; i < records.length; i++) {
                    if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                        for(var j = 0; j < records[i].propertyImages.length; j++) {
                            records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                        }
                    }
                }
                $scope.myProperties = response.records;
                // console.log($scope.myProperties);
            } else {
                $scope.noProperties = true;
            }
        } else {
            $scope.noProperties = true;
            console.log("Error in Fetching Properties");
        }
    });

    $scope.goToDetailsPage = function(propertyID) {
        if($scope.userType === 'Owner') {
            window.location.href="#!/PropertyDetails?propID="+propertyID;
            window.location.reload();
        }
    }
    $scope.goToEditPage = function(propertyID) {
        if($scope.userType === 'Owner') {
            window.location.href="#!/EditProperty?propID="+propertyID;
            window.location.reload();
        }
    }

    $scope.deleteProperty = function(propertyID) {
        swal({
            title: "Are you sure?",
            text: "Property will be deleted completely.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete!",
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: true,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                var postData = '{"propertyID":"'+propertyID+'"}';
                var req = {
                    method: 'POST',
                    url: domainAddress+'DeleteProperty',
                    data: postData
                }
                $http(req).then(function(resp) {
                    var response = resp.data;
                    if(response.status === "success"){
                        location.reload();
                    } else {
                        console.log("error in request");
                    }
                });
            } else {
                swal("Cancelled", "Your Property file is safe :)", "error");
            }
        });
    }
    $scope.goToAppointments = function(appointments) {
        $scope.appointments = appointments;
        $("#modal-appointments").modal("toggle");
    }

    $scope.acceptRejectAppointments = function(reqType,appointmentID) {
        var postData = '{"appointmentID":"'+appointmentID+'","appointmentStatus":"'+reqType+'"}';
        console.log(postData);
        var req = {
            method: 'POST',
            url: domainAddress+'UpdateAppointmentStatus',
            data: postData
        }
        $http(req).then(function(resp) {
            var response = resp.data;
            console.log(response);
            if(response.status === "success"){
                var appointments = $scope.appointments;
                for(var i = 0; i < appointments.length; i++) {
                    if(appointments[i].appointmentID == appointmentID) {
                        appointments[i].appointmentStatus = reqType;
                    }
                }
                // if(reqType == 'Accept') {
                //     swal("Success","Appointment request Accpeted.!","success");
                // } else {
                //     swal("Success","Appointment request Rejected.!","success");
                // }
            } else {
                console.log(response);
            }
        });
    }

    $scope.textSearchFunction = function(searchText) {
        if(searchText != undefined && searchText != '') {
            var req = {
                method: 'POST',
                url: domainAddress+'GlobalSearchPropertyOwner/'+searchText+'/'+$scope.userID,
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    console.log(records);
                    if(response.record_count > 0) {
                        $scope.noProperties = false;
                        for(var i = 0; i < records.length; i++) {
                            if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                                for(var j = 0; j < records[i].propertyImages.length; j++) {
                                    records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                                }
                            }
                        }
                        $scope.myProperties = records;
                        // console.log($scope.myProperties);
                    } else {
                        $scope.noProperties = true;
                    }
                } else {
                    $scope.noProperties = true;
                    console.log("Error in Fetching Properties");
                }
            });
        } else {
            var req = {
                method: 'POST',
                url: domainAddress+'GetMyPropertiesForUser/'+$scope.userID,
            }
            $http(req).then(function(resp) {
                var response = resp.data;
                if(response.status === "success"){
                    var records = response.records;
                    if(response.record_count > 0) {
                        $scope.noProperties = false;
                        for(var i = 0; i < records.length; i++) {
                            if(records[i].propertyImages != null && records[i].propertyImages != undefined) {
                                for(var j = 0; j < records[i].propertyImages.length; j++) {
                                    records[i].propertyImages[j] = domainAddress+records[i].propertyImages[j];
                                }
                            }
                        }
                        $scope.myProperties = response.records;
                        // console.log($scope.myProperties);
                    } else {
                        $scope.noProperties = true;
                    }
                } else {
                    $scope.noProperties = true;
                    console.log("Error in Fetching Properties");
                }
            });
        }
    }
});