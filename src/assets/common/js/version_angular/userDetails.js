app.controller('userDetailsController', function($timeout, $http, $rootScope, $window, $scope, $sce) {
   
    $scope.userDetails = JSON.parse(localStorage.getItem("CurrentUserDetails"));    
    
    if($scope.userDetails.propertyDetails != undefined && $scope.userDetails.propertyDetails.length > 0) {
        $scope.properties = $scope.userDetails.propertyDetails;
        for(var i = 0; i < $scope.properties.length; i++) {
            var prop = $scope.properties[i];
            if(prop.propertyImages != undefined && prop.propertyImages.length > 0) {
                for(var j = 0; j < prop.propertyImages.length; j++) {
                    prop.propertyImages[j] = domainAddress+prop.propertyImages[j];
                }
            }
        }
    }
    $scope.gotoPropertyDetails = function(propertyID) {
        console.log(propertyID);
        window.location.href="#!/PropertyDetails?propID="+propertyID;
        window.reload();    
    }
});